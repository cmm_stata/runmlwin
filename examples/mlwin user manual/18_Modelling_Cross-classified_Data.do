****************************************************************************
*     MLwiN User Manual 
*
* 18  Modelling Cross-classified Data                                    271
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 18.1 An introduction to cross-classification . . . . . . . . . . . . . 271

* 18.2 How cross-classified models are implemented in MLwiN . . . . . . .273

* 18.3 Some computational considerations . . . . . . . . . . . . . . . . 273



* 18.4 Modelling a two-way classification: An example . . . . . . . . . .275

use "https://www.bristol.ac.uk/cmm/media/runmlwin/xc.dta", clear

describe

tabulate sid, generate(s)

local c = 1

forvalues s=2/19 {

	constraint define `c' [RP3]var(s1) = [RP3]var(s`s')

	local c = `c' + 1	

}

runmlwin attain cons, ///
	level3(cons: s1-s19, diagonal) ///
	level2(pid: cons) level1(pupil: cons) ///
	constraints(1/18) nopause




* 18.5 Other aspects of the SETX command . . . . . . . . . . . . . . . . 277

forvalues s=1/19 {

	generate s`s'Xvrq = s`s'*vrq

}

local c = 19

forvalues s=2/19 {

	constraint define `c' [RP3]var(s1Xvrq) = [RP3]var(s`s'Xvrq)

	local c = `c' + 1

}

runmlwin attain cons, ///
	level3(cons: s1-s19 s1Xvrq-s19Xvrq, diagonal) ///
	level2(pid: cons) level1(pupil: cons) ///
	constraints(1/36) nopause

// Note: The final models in this section of the manual are for demonstration only.
// The models presented in the manual do not converge with the current data.
// We have therefore not given the runmlwin commands for these models.



* 18.6 Reducing storage overhead by grouping . . . . . . . . . . . . . . 279

use "https://www.bristol.ac.uk/cmm/media/runmlwin/xc.dta", clear

ssc install supclust

supclust sid pid, generate(region)

drop region

bysort sid pid: generate numchildren = _N

drop if numchildren<3

supclust sid pid, generate(region)

bysort region sid: generate rsid = 1 if _n==1

bysort region (sid): replace rsid = sum(rsid)

tabulate rsid, generate(rs)

local c = 1

forvalues s=2/8 {

	constraint define `c' [RP3]var(rs1) = [RP3]var(rs`s')

	local c = `c' + 1	

}

sort region pid pupil

runmlwin attain cons, ///
	level3(region: rs1-rs8, diagonal) ///
	level2(pid: cons) level1(pupil: cons) ///
	constraints(1/7) nopause



* 18.7 Modelling a multi-way cross-classification . . . . . . . . . . . .280

* 18.8 MLwiN commands for cross-classifications . . . . . . . . . . . . .281

* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .282



****************************************************************************
exit
