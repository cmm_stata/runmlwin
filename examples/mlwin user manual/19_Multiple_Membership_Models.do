****************************************************************************
*     MLwiN User Manual 
*
* 19  Multiple Membership Models                                         283
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 19.1 A simple multiple membership model . . . . . . . . . . . . . . . .283

* 19.2 MLwiN commands for multiple membership models . . . . . . . . . . 286

* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .286



****************************************************************************
exit