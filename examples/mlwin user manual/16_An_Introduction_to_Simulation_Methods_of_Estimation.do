****************************************************************************
*     MLwiN User Manual 
*
* 16  An Introduction to Simulation Methods of Estimation                241
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 16.1 An illustration of parameter estimation with Normally distributed . . 
* . . .data . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .242

use "https://www.bristol.ac.uk/cmm/media/runmlwin/height.dta", clear

summarize height

histogram height

quietly summarize height

display 1 - normal((200 - r(mean))/r(sd))

capture program drop heightsim1

program define heightsim1, rclass

	drop _all

	set obs 100

	generate heightsim = 175.35 + 10.002*invnorm(uniform())

	summarize heightsim

	return scalar pmean = r(mean)

	return scalar pvar  = r(Var)
end

simulate, reps(1000) seed(12345): heightsim1
// Note: To obtain estimates as close as possible to the manual, ncrease the
// number of reps to 10000.

generate iteration = _n

line pmean iteration

kdensity pmean

centile pmean, centile(2.5 97.5)

line pvar iteration

kdensity pvar

centile pvar, centile(2.5 97.5)



use "https://www.bristol.ac.uk/cmm/media/runmlwin/height.dta", clear

save "height.dta", replace

capture program drop heightsim2

program define heightsim2, rclass

	use "height.dta", clear

	bsample

	summarize height

	return scalar npmean = r(mean)

	return scalar npvar  = r(Var)
end

simulate, reps(1000) seed(12345): heightsim2
// Note: To obtain estimates as close as possible to the manual, increase the
// number of reps to 10000.

erase "height.dta"

generate iteration = _n

line npmean iteration

kdensity npmean

centile npmean, centile(2.5 97.5)

line npvar iteration

kdensity npvar

centile npvar, centile(2.5 97.5)



* 16.2 Generating random numbers in MLwiN . . . . . . . . . . . . . . . .249

clear

set obs 100

generate female = (uniform()<=0.6)

generate height2 = (1 - female)*(175 + 10*invnorm(uniform())) + female*(160 + 8*invnorm(uniform()))

histogram height2, by(female)



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .253



****************************************************************************
exit