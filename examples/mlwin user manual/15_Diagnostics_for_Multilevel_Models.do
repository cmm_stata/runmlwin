****************************************************************************
*     MLwiN User Manual 
*
* 15  Diagnostics for Multilevel Models                                  225
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 15.1 Introduction . . . . . . . . . . . . . . . . . . . . . . . . . . .225

use "https://www.bristol.ac.uk/cmm/media/runmlwin/diag1.dta", clear

runmlwin n_ilea cons n_vrq, ///
	level2(school: cons n_vrq, ///
		residuals(u, standardised leverage influence deletion) ///
	) ///
	level1(pupil: cons, residuals(e)) nopause

predict xb

generate yhat = xb + u0 + u1*n_vrq

drop xb

twoway (line yhat n_vrq if school~=17, connect(a)) ///
	(line yhat n_vrq if school==17, lwidth(*3)), ///
	legend(off)

rename yhat yhatold

twoway (scatter n_ilea n_vrq if school~=17) ///
	(scatter n_ilea n_vrq if school==17, msize(*2)), ///
	legend(off)

egen pickone = tag(school)

egen u0rank = rank(u0) if pickone==1

egen u1rank = rank(u1) if pickone==1

serrbar u0 u0se u0rank if pickone==1, scale(1.4) yline(0) ///
	addplot(scatter u0 u0rank if pickone==1 & school==17, ///
	mcolor(maroon) msize(3)) legend(off)

serrbar u1 u1se u1rank if pickone==1, scale(1.4) yline(0) ///
	addplot(scatter u1 u1rank if pickone==1 & school==17, ///
	mcolor(maroon) msize(3)) legend(off)

twoway (scatter u1 u0 if pickone==1 & school~=17) ///
	(scatter u1 u0 if pickone==1 & school==17, msize(3)), legend(off)



* 15.2 Diagnostics plotting: Deletion residuals, influence and leverage . 231

histogram u0 if pickone==1, width(0.01)

histogram u0std if pickone==1, width(0.1)

histogram u0lev if pickone==1, width(0.01)

histogram u0inf if pickone==1, width(0.025)

histogram u0del if pickone==1, width(0.1)

twoway (scatter u0lev u0std if pickone==1 & school~=17) ///
	(scatter u0lev u0std if pickone==1 & school==17, msize(3)), legend(off)

histogram u1 if pickone==1, width(0.01)

histogram u1std if pickone==1, width(0.1)

histogram u1lev if pickone==1, width(0.01)

histogram u1inf if pickone==1, width(0.025)

histogram u1del if pickone==1, width(0.1)

twoway (scatter u1lev u1std if pickone==1 & school~=17) ///
	(scatter u1lev u1std if pickone==1 & school==17, msize(3)), legend(off)

egen e0rank = rank(e0)

summarize e0

generate e0std = (e0 - r(mean))/r(sd)

generate e0uniform = e0rank/(r(N) + 1)

generate e0nscore = invnorm(e0uniform)

twoway (scatter e0std e0nscore if school~=17) ///
	(scatter e0std e0nscore if school==17, msize(3)), ///
	yline(0) xline(0) ylabel(-4(1)4) xlabel(-4(1)4) aspectratio(1)

bysort school (pupil): generate pupilnumber = _n

sort e0

list school pupil pupilnumber if _n==1

generate s17p22 = (school==17 & pupilnumber==22)

drop u0* u1* e0*

sort school pupil

runmlwin n_ilea cons n_vrq s17p22, ///
	level2(school: cons n_vrq, residuals(u)) ///
	level1(pupil: cons) initsprevious nopause

egen u0rank = rank(u0) if pickone==1

egen u1rank = rank(u1) if pickone==1

serrbar u0 u0se u0rank if pickone==1, scale(1.4) yline(0) ///
	addplot(scatter u0 u0rank if pickone==1 & school==17, ///
	mcolor(maroon) msize(3)) ///
	legend(off)

serrbar u1 u1se u1rank if pickone==1, scale(1.4) yline(0) ///
	addplot(scatter u1 u1rank if pickone==1 & school==17, ///
	mcolor(maroon) msize(3)) ///
	legend(off)

generate s17 = (school==17)

generate s17Xn_vrq = s17*n_vrq

drop u0* u1*

runmlwin n_ilea cons n_vrq s17p22 s17 s17Xn_vrq, ///
	level2(school: cons n_vrq) ///
	level1(pupil: cons) initsprevious nopause
	
runmlwin n_ilea cons n_vrq s17p22 s17, ///
	level2(school: cons n_vrq, residuals(u)) ///
	level1(pupil: cons) initsprevious nopause

predict xb

gen yhat = xb + u0 + u1*n_vrq
	
twoway (line yhatold n_vrq if school~=17, connect(a)) ///
	(line yhatold n_vrq if school==17, lwidth(*3)), ///
	legend(off)

twoway (scatter yhat n_vrq if school~=17) ///
	(scatter yhat n_vrq if school==17, msize(*2)), ///
	legend(off)



* 15.3 A general approach to data exploration . . . . . . . . . . . . . .240

* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .240



****************************************************************************
exit