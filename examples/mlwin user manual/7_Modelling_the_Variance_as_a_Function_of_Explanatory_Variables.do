********************************************************************************
*     MLwiN User Manual 
*
* 7   Modelling the Variance as a Function of Explanatory Variables           89
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012). A User's 
*     Guide to MLwiN, v2.26. Centre for Multilevel Modelling, University of 
*     Bristol.
********************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
********************************************************************************

* 7.1 A level 1 variance function for two groups . . . . . . . . . . . . . . .89

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

generate boy = 1 - girl

runmlwin normexam boy girl, level1(student: boy girl, diagonal) nopause



* 7.2 Variance functions at level 2 . . . . . . . . . . . . . . . . . . . . . 95

runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) level1(student: cons) nopause

generate l2varfn = [RP2]var(cons) ///
  + 2*[RP2]cov(cons\standlrt)*standlrt ///
  + [RP2]var(standlrt)*standlrt^2

line l2varfn standlrt, sort



* 7.3 Further elaborating the model for the student-level variance . . . . . .99

runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) level1(student: cons standlrt) nopause

replace l2varfn = [RP2]var(cons) ///
  + 2*[RP2]cov(cons\standlrt)*standlrt ///
  + [RP2]var(standlrt)*standlrt^2

generate l1varfn = [RP1]var(cons) ///
  + 2*[RP1]cov(cons\standlrt)*standlrt ///
  + [RP1]var(standlrt)*standlrt^2

line l2varfn l1varfn standlrt, sort

matrix A = (1,1,0,0,0,1)

runmlwin normexam cons standlrt girl, ///
	level2(school: cons standlrt) ///
	level1(student: cons standlrt girl, elements(A)) nopause

matrix A = (1,1,0,0,1,1)

runmlwin normexam cons standlrt girl, ///
	level2(school: cons standlrt) ///
	level1(student: cons standlrt girl, elements(A)) nopause

replace l2varfn = [RP2]var(cons) ///
  + 2*[RP2]cov(cons\standlrt)*standlrt ///
  + [RP2]var(standlrt)*standlrt^2

generate l1varfnboys = [RP1]var(cons) ///
  + 2*[RP1]cov(cons\standlrt)*standlrt

generate l1varfngirls = [RP1]var(cons) ///
  + (2*[RP1]cov(cons\standlrt) ///
  + 2*[RP1]cov(standlrt\girl))*standlrt + [RP1]var(girl)

line l2varfn l1varfnboys l1varfngirls standlrt, sort



*     Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .106



********************************************************************************
exit
