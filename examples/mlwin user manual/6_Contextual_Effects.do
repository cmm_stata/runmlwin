****************************************************************************
*     MLwiN User Manual 
*
* 6   Contextual Effects                                                  79
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) level1(student: cons) nopause



* 6.1 The impact of school gender on girls' achievement . . . . . . . . . 80

generate boysch = (schgend==2)

generate girlsch = (schgend==3)

runmlwin normexam cons standlrt girl boysch girlsch, ///
	level2(school: cons standlrt) ///
	level1(student: cons) nopause

generate boyschXstandlrt = boysch*standlrt

generate girlschXstandlrt = girlsch*standlrt

runmlwin normexam cons standlrt girl boysch girlsch ///
	boyschXstandlrt girlschXstandlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) initsprevious nopause



* 6.2 Contextual effects of school intake ability averages . . . . . . . .83

generate mid = (schav==2)

generate high = (schav==3)

runmlwin normexam cons standlrt girl boysch girlsch mid high, ///
	level2(school: cons standlrt) ///
	level1(student: cons) nopause

generate standlrtXmid = standlrt*mid

generate standlrtXhigh = standlrt*high

runmlwin normexam cons standlrt girl boysch girlsch ///
	mid high standlrtXmid standlrtXhigh, ///
	level2(school: cons standlrt) ///
	level1(student: cons) nopause

generate temp = standlrt



foreach var of varlist cons standlrt girl boysch girlsch mid standlrtXmid {
	
	replace `var' = 0

}

predict hilodiff

predict hilodiff_se, stdp

generate hilodiff_lo = hilodiff - 1.96*hilodiff_se

generate hilodiff_hi = hilodiff + 1.96*hilodiff_se

replace standlrt = temp

keep if high==1

keep high standlrtXhigh hilodiff hilodiff_lo hilodiff_hi

duplicates drop

sort standlrt

line hilodiff standlrt

twoway (line hilodiff standlrt) ///
	(line hilodiff_lo standlrt, lcolor(dknavy) lpattern(dash)) ///
	(line hilodiff_hi standlrt, lcolor(dknavy) lpattern(dash)), ///
	legend(off)



*     Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . 87



****************************************************************************
exit