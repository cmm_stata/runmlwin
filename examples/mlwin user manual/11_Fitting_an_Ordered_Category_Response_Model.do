****************************************************************************
*     MLwiN User Manual 
*
* 11  Fitting an Ordered Category Response Model                         161
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 11.1 Introduction . . . . . . . . . . . . . . . . . . . . . . . . . . .161

use "https://www.bristol.ac.uk/cmm/media/runmlwin/alevchem.dta", clear

describe



* 11.2 An analysis using the traditional approach . . . . . . . . . . . .162

histogram a_point, discrete frequency gap(50) xlabel(1(1)6, valuelabel)

egen a_point_rank = rank(a_point)

generate a_point_uniform = (a_point_rank - 0.5)/_N

generate alevelnormal = invnorm(a_point_uniform)

runmlwin alevelnormal cons, level1(pupil: cons) nopause

generate gcseav = gcse_tot/gcse_no

egen gcseav_rank = rank(gcseav)

generate gcseav_uniform = (gcseav_rank - 0.5)/_N

generate gcseavnormal = invnorm(gcseav_uniform)

generate gcse2 = gcseavnormal^2

generate gcse3 = gcseavnormal^3

rename gender female

runmlwin alevelnormal cons female gcseavnormal gcse2 gcse3, ///
	level1(pupil: cons) nopause

runmlwin alevelnormal cons female gcseavnormal gcse2, ///
	level1(pupil: cons) nopause

runmlwin alevelnormal cons female, level1(pupil: cons) nopause

runmlwin gcseavnormal cons female, level1(pupil: cons) nopause



* 11.3 A single-level model with an ordered categorical response variable 166

runmlwin a_point cons, ///
	level1(pupil) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6)) nopause
	

* 11.4 A two-level model . . . . . . . . . . . . . . . . . . . . . . . . 171
egen school = group(lea estab)
// Note: Establishment codes on their own do not uniquely identify schools.
// Schools are instead uniquely identified by LEA code, establishment ID 
// combination. Thus, here we generated a unique school ID.

runmlwin a_point cons, ///
	level2(school: (cons, contrast(1/5))) ///
	level1(pupil) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6)) ///
	nopause
// Note: Here we get slightly different results to those presented in the
// manual. The results presented here are correct. The results presented in
// the manual are incorrect. The error in the manual related to the fact
// that that LEAs 311 and 315 appear consecutively in the dataset and that
// each LEA only has one school and the ID for those two schools is the
// same. (The estab ID for both schools is 8000). In sum, two distinct
// schools appear in the data consecutively with the same Estab ID. MLwiN
// will incorectly treat these two schools as being the same establishment.
// The creation and use of the school ID above avoids this problem as now
// every schools has a unique ID. This issue applies to all the models in
// this chapter and so you  will see slightly different results to those
// presented in the manual for all the remaining models in this chapter.

runmlwin a_point cons, ///
	level2(school: (cons, contrast(1/5))) ///
	level1(pupil) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6) pql2) nopause

runmlwin a_point cons (gcseavnormal, contrast(1/5)), ///
	level2(school: (cons, contrast(1/5))) ///
	level1(pupil) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6) pql2) nopause

runmlwin a_point cons gcseavnormal, ///
	level2(school: (cons, contrast(1/5))) ///
	level1(pupil) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6) pql2) nopause
	
runmlwin a_point cons (gcseavnormal female gcse2, contrast(1/5)), ///
	level2(school: (cons, contrast(1/5))) ///
	level1(pupil) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6) pql2) nopause

runmlwin a_point cons (gcseavnormal female gcse2, contrast(1/5)), ///
	level2(school: (cons gcseavnormal, contrast(1/5))) ///
	level1(pupil) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6) pql2) ///
	initsprevious nopause

display invlogit([FP1]cons_1)

display invlogit([FP2]cons_2)

display invlogit([FP3]cons_3)

display invlogit([FP4]cons_4)

display invlogit([FP5]cons_5)


display invlogit([FP1]cons_1 + [FP6]gcseavnormal_12345)

display invlogit([FP2]cons_2 + [FP6]gcseavnormal_12345)

display invlogit([FP3]cons_3 + [FP6]gcseavnormal_12345)

display invlogit([FP4]cons_4 + [FP6]gcseavnormal_12345)

display invlogit([FP5]cons_5 + [FP6]gcseavnormal_12345)

runmlwin a_point cons (gcseavnormal female gcse2, contrast(1/5)), ///
	level2(school: (cons gcseavnormal female, contrast(1/5))) ///
	level1(pupil) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6) pql2) ///
	initsprevious nopause



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .180



****************************************************************************
exit