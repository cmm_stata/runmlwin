****************************************************************************
*     MLwiN User Manual 
*
* 12  Modelling Count Data                                               181
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 12.1 Introduction . . . . . . . . . . . . . . . . . . . . . . . . . . .181

use "https://www.bristol.ac.uk/cmm/media/runmlwin/mmmec.dta", clear

describe



* 12.2 Fitting a simple Poisson model . . . . . . . . . . . . . . . . . .182

generate lnexpected = ln(exp)

runmlwin obs cons uvbi, ///
	level1(county) ///
	discrete(distribution(poisson) offset(lnexpected)) nopause



* 12.3 A three-level analysis . . . . . . . . . . . . . . . . . . . . . .184

runmlwin obs cons, ///
	level3(nation: cons) level2(region: cons) level1(county) ///
	discrete(distribution(poisson) offset(lnexpected)) rigls nopause

runmlwin obs cons, ///
	level3(nation: cons) level2(region: cons) level1(county) ///
	discrete(distribution(poisson) offset(lnexpected) pql2) rigls nopause

runmlwin obs cons uvbi, ///
	level3(nation: cons) level2(region: cons) level1(county) ///
	discrete(distribution(poisson) offset(lnexpected) pql2) rigls nopause



* 12.4 A two-level model using separate country terms . . . . . . . . . .186

tabulate nation

generate belgium     = (nation==1)

generate wgermany    = (nation==2)

generate denmark     = (nation==3)

generate france      = (nation==4)

generate uk          = (nation==5)

generate italy       = (nation==6)

generate ireland     = (nation==7)

generate luxembourg  = (nation==8)

generate netherlands = (nation==9)

foreach var of varlist belgium-netherlands {

	generate `var'Xuvbi = `var'*uvbi

}

runmlwin obs belgium-netherlands belgiumXuvbi-netherlandsXuvbi, ///
	level2(region: cons) level1(county) ///
	discrete(distribution(poisson) offset(lnexpected) pql2) ///
	rigls nopause

predict xb

twoway ///
	(line xb uvbi if belgium==1) ///
	(line xb uvbi if wgermany==1) ///
	(line xb uvbi if denmark==1) ///
	(line xb uvbi if france==1) ///
	(line xb uvbi if uk==1) ///
	(line xb uvbi if italy==1) ///
	(line xb uvbi if ireland==1) ///
	(line xb uvbi if luxembourg==1) ///
	(line xb uvbi if netherlands==1), ///
	legend(position(3) col(1) order( ///
		1 "Belgium" ///
		2 "W Germany" ///
		3 "Denmark" ///
		4 "France" ///
		5 "UK" ///
		6 "Italy" ///
		7 "Ireland" ///
		8 "Luxembourg" ///
		9 "Netherlands"))


* 12.5 Some issues and problems for discrete response models . . . . . . 190

* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .190



****************************************************************************
exit
