****************************************************************************
*     MLwiN User Manual 
*
* 1   Introducing Multilevel Models                                        1
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 1.1 Multilevel data structures . . . . . . . . . . . . . . . . . . . . . 1

* 1.2 Consequences of ignoring a multilevel structure . . . . . . . . . . .2

* 1.3 Levels of a data structure . . . . . . . . . . . . . . . . . . . . . 3

* 1.4 An introductory description of multilevel modelling . . . . . . . . .6



****************************************************************************
exit