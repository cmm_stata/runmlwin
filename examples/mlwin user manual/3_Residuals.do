****************************************************************************
*     MLwiN User Manual 
*
* 3   Residuals 						          37
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 3.1 What are multilevel residuals? . . . . . . . . . . . . . . . . . . .37

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

runmlwin normexam cons, ///
	level2(school: cons, residuals(u)) ///
	level1(student: cons, residuals(e)) ///
	nopause



* 3.2 Calculating residuals in MLwiN . . . . . . . . . . . . . . . . . . .40

egen pickone = tag(school)

egen u0rank = rank(u0) if pickone==1

serrbar u0 u0se u0rank if pickone==1, scale(1.96) yline(0)



* 3.3 Normal plots . . . . . . . . . . . . . . . . . . . . . . . . . . . .43

summarize e0

generate e0std = (e0 - r(mean))/r(sd)

egen e0rank = rank(e0)

generate e0uniform = (e0rank - 0.5)/_N

generate e0nscore = invnorm(e0uniform)

scatter e0std e0nscore, ///
	yline(0) xline(0) ylabel(-4(1)4) xlabel(-4(1)4) aspectratio(1)

keep if pickone==1

keep u0 u0rank

summarize u0

generate u0std = (u0 - r(mean))/r(sd)

generate u0uniform = (u0rank - 0.5)/_N

generate u0nscore = invnorm(u0uniform)

scatter u0std u0nscore, ///
	yline(0) xline(0) ylabel(-3(1)3) xlabel(-3(1)3) aspectratio(1)

save "tutorial3.dta", replace



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . . 45



****************************************************************************
exit
