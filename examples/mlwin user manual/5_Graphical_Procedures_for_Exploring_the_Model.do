****************************************************************************
*     MLwiN User Manual 
*
* 5   Graphical Procedures for Exploring the Model                        65
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 5.1 Displaying multiple graphs . . . . . . . . . . . . . . . . . . . . .65

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

runmlwin normexam cons standlrt, ///
	level2(school: cons, residuals(u)) ///
	level1(student: cons) nopause

egen pickone = tag(school)

egen u0rank = rank(u0) if pickone==1

serrbar u0 u0se u0rank if pickone==1, scale(1.96) yline(0)

scatter normexam standlrt, ///
	yline(0) xline(0) ylabel(-4(1)4) xlabel(-4(1)4) aspectratio(1)



* 5.2 Highlighting in graphs . . . . . . . . . . . . . . . . . . . . . . .68

predict xb

generate xbu = xb + u0

sort school standlrt

line xbu standlrt, connect(a)

twoway (line xbu standlrt, connect(a)) ///
	(line xb standlrt, lwidth(*3) lcolor(black))

list school u0rank if pickone==1 & u0rank==65
	
serrbar u0 u0se u0rank if pickone==1, scale(1.96) yline(0) ///
	addplot( ///
		(scatter u0 u0rank if pickone==1 & school==53, msize(*2) mcolor(maroon)) ///	
	) legend(order(3 "School 53"))

twoway (line xbu standlrt, connect(a)) ///
	(line xb standlrt, lwidth(*3) lcolor(black)) ///
	(line xbu standlrt if school==53, lwidth(*2) lcolor(maroon)), ///
	legend(order(3 "School 53"))

twoway (scatter normexam standlrt) ///
	(scatter normexam standlrt if school==53, msize(*2) mcolor(maroon)), ///
	legend(order(2 "School 53"))

bysort school: generate size = _N

list school size u0rank if pickone==1 & inrange(u0rank,28,32)

serrbar u0 u0se u0rank if pickone==1, scale(1.96) yline(0) ///
	addplot( ///
		(scatter u0 u0rank if pickone==1 & school==53, msize(*2) mcolor(maroon)) ///	
		(scatter u0 u0rank if pickone==1 & school==48, msize(*2) mcolor(green)) ///
	) legend(order(3 "School 53" 4 "School 48"))

twoway (line xbu standlrt, connect(a)) ///
	(line xb standlrt, lwidth(*3) lcolor(black)) ///
	(line xbu standlrt if school==53, lwidth(*3) lcolor(maroon)) ///
	(line xbu standlrt if school==48, lwidth(*3) lcolor(green)), ///
	legend(order(2 "The average school" 3 "School 53" 4 "School 48"))

twoway (scatter normexam standlrt) ///
	(scatter normexam standlrt if school==53, msize(*2) mcolor(maroon)) ///
	(scatter normexam standlrt if school==48, msize(*2) mcolor(green)), ///
	legend(order(2 "School 53" 3 "School 48"))

list school u0rank if pickone==1 & u0rank==1

serrbar u0 u0se u0rank if pickone==1, scale(1.96) yline(0) ///
	addplot( ///
		(scatter u0 u0rank if pickone==1 & school==53, msize(*2) mcolor(maroon)) ///	
		(scatter u0 u0rank if pickone==1 & school==59, msize(*2) mcolor(green)) ///
	) legend(order(3 "School 53" 4 "School 59"))

twoway (line xbu standlrt, connect(a)) ///
	(line xb standlrt, lwidth(*3) lcolor(black)) ///
	(line xbu standlrt if school==53, lwidth(*3) lcolor(maroon)) ///
	(line xbu standlrt if school==59, lwidth(*3) lcolor(green)), ///
	legend(order(2 "The average school" 3 "School 53" 4 "School 59"))

twoway (scatter normexam standlrt) ///
	(scatter normexam standlrt if school==53, msize(*2) mcolor(maroon)) ///
	(scatter normexam standlrt if school==59, msize(*2) mcolor(green)), ///
	legend(order(2 "School 53" 3 "School 59"))

drop xb xbu u0 u0 u0se u0rank
	
runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt, residuals(u, sampling)) ///
	level1(student: cons) nopause

predict xb

generate xbu = xb + u0 + u1*standlrt

twoway (scatter u0 u1) ///
	(scatter u0 u1 if pickone==1 & school==53, msize(*2) mcolor(maroon)) ///
	(scatter u0 u1 if pickone==1 & school==59, msize(*2) mcolor(green)), ///
	ytitle("Intercept") xtitle("Slope") ///
	yline(0) xline(0) ylabel(-1(.25)1) xlabel(-1(.25)1) aspectratio(1) ///
	legend(order(2 "School 53" 3 "School 59"))

twoway (line xbu standlrt, connect(a)) ///
	(line xb standlrt, lwidth(*3) lcolor(black)) ///
	(line xbu standlrt if school==53, lwidth(*3) lcolor(maroon)) ///
	(line xbu standlrt if school==59, lwidth(*3) lcolor(green)), ///
	legend(order(3 "School 53" 4 "School 59"))

twoway (scatter normexam standlrt) ///
	(scatter normexam standlrt if school==53, msize(*2) mcolor(maroon)) ///
	(scatter normexam standlrt if school==59, msize(*2) mcolor(green)), ///
	legend(order(2 "School 53" 3 "School 59"))

generate xbu_lo = xbu - 1.96*sqrt(u0var + 2*u0u1cov*standlrt + u1var*standlrt^2)

generate xbu_hi = xbu + 1.96*sqrt(u0var + 2*u0u1cov*standlrt + u1var*standlrt^2) 
 
twoway (line xb standlrt, lwidth(*3) lcolor(black)) ///
	(line xbu standlrt if school==53, lwidth(*3) lcolor(maroon)) ///
	(line xbu_lo standlrt if school==53, lcolor(maroon) lpattern(dash)) ///
	(line xbu_hi standlrt if school==53, lcolor(maroon) lpattern(dash)) ///
	(line xbu standlrt if school==59, lwidth(*3) lcolor(green)) ///
	(line xbu_lo standlrt if school==59, lcolor(green) lpattern(dash)) ///
	(line xbu_hi standlrt if school==59, lcolor(green) lpattern(dash)), ///
	legend(order(2 "School 53" 5 "School 59"))



*     Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . 77



****************************************************************************
exit
