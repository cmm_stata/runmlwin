********************************************************************************
*     MLwiN User Manual 
*
* 14  Multivariate Response Models                                           211
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012). A User's 
*     Guide to MLwiN, v2.26. Centre for Multilevel Modelling, University of 
*     Bristol.
********************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
********************************************************************************

* 14.1 Introduction . . . . . . . . . . . . . . . . . . . . . . . . . . . . .211

use "https://www.bristol.ac.uk/cmm/media/runmlwin/gcsemv1.dta", clear

describe



* 14.2 Specifying a multivariate model . . . . . . . . . . . . . . . . . . . 212



* 14.3 Setting up the basic model . . . . . . . . . . . . . . . . . . . . . .214

runmlwin (written cons, eq(1)) (csework cons, eq(2)), ///
	level1(student: (cons, eq(1)) (cons, eq(2))) nosort nopause

runmlwin (written cons female, eq(1)) (csework cons female, eq(2)), ///
	level2(school: (cons, eq(1)) (cons, eq(2))) ///
	level1(student: (cons, eq(1)) (cons, eq(2))) nopause

display [RP2]cov(cons_1\cons_2)/sqrt([RP2]var(cons_1)*[RP2]var(cons_2))

display [RP1]cov(cons_1\cons_2)/sqrt([RP1]var(cons_1)*[RP1]var(cons_2))



* 14.4 A more elaborate model . . . . . . . . . . . . . . . . . . . . . . . .219
runmlwin (written cons female, eq(1)) (csework cons female, eq(2)), ///
	level2(school: (cons female, eq(1)) (cons female, eq(2))) ///
	level1(student: (cons, eq(1)) (cons, eq(2))) nopause
// Note: The parameter estimates are correct, but note that variables in the
//  random part are presented in a different order to that in the manual.

runmlwin (written cons female, eq(1)) (csework cons female, eq(2)), ///
	level2(school: (cons, eq(1)) (cons female, eq(2)), residuals(u)) ///
	level1(student: (cons, eq(1)) (cons, eq(2))) nopause

display [RP2]cov(cons_1\cons_2)/sqrt([RP2]var(cons_1)*[RP2]var(cons_2))

display [RP2]cov(cons_1\female_2)/sqrt([RP2]var(cons_1)*[RP2]var(female_2))

display [RP2]cov(cons_2\female_2)/sqrt([RP2]var(cons_2)*[RP2]var(female_2))

graph matrix u0 u1 u2, half



* 14.5 Multivariate models for discrete responses . . . . . . . . . . . . . .222

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

generate binexam = (normexam>0)

generate binlrt = (standlrt>0)

runmlwin (binexam cons, eq(1)) (binlrt cons, eq(2)), ///
	level1(student:) ///
	discrete(distribution(binomial binomial) denominator(cons cons)) ///
	nosort nopause



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . . . .224



********************************************************************************
exit
