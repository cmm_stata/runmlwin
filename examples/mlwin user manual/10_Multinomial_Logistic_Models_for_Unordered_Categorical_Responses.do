********************************************************************************
*     MLwiN User Manual 
*
* 10  Multinomial Logistic Models for Unordered Categorical Responses        145
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012). A User's 
*     Guide to MLwiN, v2.26. Centre for Multilevel Modelling, University of 
*     Bristol.
********************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
********************************************************************************

* 10.1 Introduction . . . . . . . . . . . . . . . . . . . . . . . . . . . . .145

use "https://www.bristol.ac.uk/cmm/media/runmlwin/bang.dta", clear

tabulate use4



* 10.2 Single-level multinomial logistic regression . . . . . . . . . . . . .146



* 10.3 Fitting a single-level multinomial logistic model in MLwiN . . . . . .147

tabulate lc use4, row

label define use4 1 "ster" 2 "mod" 3 "trad" 4 "none", modify

label values use4 use4

generate lc1 = (lc==1)

generate lc2 = (lc==2)

generate lc3plus = (lc==3)

runmlwin use4 cons lc1 lc2 lc3plus, ///
	level1(woman) ///
	discrete(distribution(multinomial) link(mlogit) denominator(cons) ///
	  basecategory(4)) ///
	nopause
// We might need to add equation labels for the fixed part equations in the 
// model output. For example "Contrast 1", "Contrast 2" and so on.

display "Pr(y = 1) = " exp([FP1]cons_1)/ ///
  (1 + exp([FP1]cons_1) + exp([FP2]cons_2) + exp([FP3]cons_3))

display "Pr(y = 2) = " exp([FP2]cons_2)/ ///
  (1 + exp([FP1]cons_1) + exp([FP2]cons_2) + exp([FP3]cons_3))

display "Pr(y = 3) = " exp([FP3]cons_3)/ ///
  (1 + exp([FP1]cons_1) + exp([FP2]cons_2) + exp([FP3]cons_3))

display "Pr(y = 4) = " 1/ ///
  (1 + exp([FP1]cons_1) + exp([FP2]cons_2) + exp([FP3]cons_3))



* 10.4 A two-level random intercept multinomial logistic regression model 154



* 10.5 Fitting a two-level random intercept model . . . . . . . . . . . . . .155

runmlwin use4 cons lc1 lc2 lc3plus, ///
	level2(district: cons) ///
	level1(woman) ///
	discrete(distribution(multinomial) link(mlogit) denominator(cons) ///
	  basecategory(4)) ///
	nopause

runmlwin use4 cons lc1 lc2 lc3plus, ///
	level2(district: cons, residuals(u)) ///
	level1(woman) ///
	discrete(distribution(multinomial) link(mlogit) denominator(cons) ///
	  basecategory(4) pql2) ///
	nopause
// Need to use previous model's estimates as starting values for this model

display %4.3f [RP2]cov(cons_1\cons_2)/sqrt([RP2]var(cons_1)*[RP2]var(cons_2))

display %4.3f [RP2]cov(cons_1\cons_3)/sqrt([RP2]var(cons_1)*[RP2]var(cons_3))

display %4.3f [RP2]cov(cons_2\cons_3)/sqrt([RP2]var(cons_2)*[RP2]var(cons_3))

egen pickone = tag(district)

egen u0rank = rank(u0) if pickone==1

egen u1rank = rank(u1) if pickone==1

egen u2rank = rank(u2) if pickone==1

serrbar u0 u0se u0rank if pickone==1, scale(1.96) yline(0)

serrbar u1 u1se u1rank if pickone==1, scale(1.96) yline(0)

serrbar u2 u2se u2rank if pickone==1, scale(1.96) yline(0)

serrbar u0 u0se u0rank if pickone==1, scale(1.96) yline(0) ///
	addplot( ///
		(scatter u0 u0rank if district==56, mcolor(maroon) msize(3)) ///
		(scatter u0 u0rank if district==11, mcolor(green) msize(3)) ///
	) legend(off)

serrbar u1 u1se u1rank if pickone==1, scale(1.96) yline(0) ///
	addplot( ///
		(scatter u1 u1rank if district==56, mcolor(maroon) msize(3)) ///
		(scatter u1 u1rank if district==11, mcolor(green) msize(3)) ///
	) legend(off)

serrbar u2 u2se u2rank if pickone==1, scale(1.96) yline(0) ///
	addplot( ///
		(scatter u2 u2rank if district==56, mcolor(maroon) msize(3)) ///
		(scatter u2 u2rank if district==11, mcolor(green) msize(3)) ///
	) legend(off)



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . . . .159



********************************************************************************
exit
