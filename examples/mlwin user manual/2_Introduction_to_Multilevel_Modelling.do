****************************************************************************
*     MLwiN User Manual 
*
* 2   Introduction to Multilevel Modelling                                 9
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 2.1 The tutorial data set . . . . . . . . . . . . . . . . . . . . . . . .9

* 2.2 Opening the worksheet and looking at the data . . . . . . . . . . . 10

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

describe

list school student normexam cons standlrt girl schgend in 1/10



* 2.3 Comparing two groups . . . . . . . . . . . . . . . . . . . . . . . .13

tabstat normexam, statistics(count mean sd) by(girl) columns(statistics)

ttest normexam, by(girl)

runmlwin normexam cons girl, level1(student: cons) nopause




* 2.4 Comparing more than two groups: Fixed effects models . . . . . . . .20

bysort school: egen mean_normexam = mean(normexam)

egen pickone = tag(school)

histogram mean_normexam if pickone==1, frequency

quietly tabulate school, generate(s)

runmlwin normexam cons s1-s64, level1(student: cons) nopause

estimates store model2

anova normexam school

quietly runmlwin normexam cons, level1(student: cons) nopause

estimates store model1

lrtest model1 model2

generate boysch = (schgend==2)

generate girlsch = (schgend==3)

runmlwin normexam cons s1-s64 boysch girlsch, level1(student: cons) nopause



* 2.5 Comparing means: Random effects or multilevel model . . .  . . . . .28

runmlwin normexam cons, level2(school: cons) level1(student: cons) nopause

runmlwin normexam cons boysch girlsch, ///
	level2(school: cons) level1(student: cons) nopause

save "tutorial2.dta", replace



*     Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . 35



****************************************************************************
exit