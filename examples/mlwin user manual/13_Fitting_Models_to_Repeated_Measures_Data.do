********************************************************************************
*     MLwiN User Manual 
*
* 13  Fitting Models to Repeated Measures Data                               191
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012). A User's 
*     Guide to MLwiN, v2.26. Centre for Multilevel Modelling, University of 
*     Bristol.
********************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
********************************************************************************

* 13.1 Introduction . . . . . . . . . . . . . . . . . . . . . . . . . . . . .191

* 13.2 A basic model . . . . . . . . . . . . . . . . . . . . . . . . . . . . 194

use "https://www.bristol.ac.uk/cmm/media/runmlwin/reading1.dta", clear

describe

summarize

recode * (-10=.)

summarize

reshape long AGE READ, i(ID) j(occasion)

describe

rename ID student

rename AGE age

rename READ reading

list in 1/5

tabstat reading, statistics(count mean sd) by(occasion) columns(statistics)

tabstat age, statistics(count mean sd) by(occasion) columns(statistics)

generate cons = 1

runmlwin reading cons, level2(student: cons) level1(occasion: cons) nopause



* 13.3 A linear growth curve model . . . . . . . . . . . . . . . . . . . . . 201

runmlwin reading cons age, ///
	level2(student: cons) level1(occasion: cons) initsprevious nopause 

runmlwin reading cons age, ///
	level2(student: cons age, residuals(u)) ///
	level1(occasion: cons, residuals(e)) ///
	initsprevious nopause

egen pickone = tag(student)

summarize u0 if pickone==1

generate u0std = (u0 - r(mean))/r(sd) if pickone==1

summarize u1 if pickone==1

generate u1std = (u1 - r(mean))/r(sd) if pickone==1

scatter u1std u0std  if pickone==1, ///
	yline(0) xline(0) ylabel(-4(1)4) xlabel(-4(1)4) aspectratio(1)

drop u0* u1*

summarize e0

generate e0std = (e0 - r(mean))/r(sd) if e(sample)

egen e0rank = rank(e0) if e(sample)

generate e0uniform = (e0rank - 0.5)/e(N) if e(sample)

generate e0nscore = invnorm(e0uniform) if e(sample)

scatter e0std e0nscore if e(sample), ///
	yline(0) xline(0) ylabel(-4(1)4) xlabel(-4(1)4) aspectratio(1)



* 13.4 Complex level 1 variation . . . . . . . . . . . . . . . . . . . . . . 204

runmlwin reading cons age, ///
	level2(student: cons age) level1(occasion: cons age) nopause



* 13.5 Repeated measures modelling of non-linear polynomial growth . . . . . 205

gen agesq = age^2

runmlwin reading cons age agesq, ///
	level2(student: cons age agesq, residuals(u)) level1(occasion: cons age) nopause

generate l2varfn = [RP2]var(cons) + 2*[RP2]cov(cons\age)*age + ///
	[RP2]var(age)*age^2 + 2*[RP2]cov(cons\agesq)*agesq ///
	+ 2*[RP2]cov(age\agesq)*age*agesq + [RP2]var(agesq)*agesq^2 

generate l1varfn = [RP1]var(cons) + 2*[RP1]cov(cons\age)*age + [RP1]var(age)*age^2

generate totvarfn = l2varfn + l1varfn

scatter totvarfn age

predict xb

gen yhat = xb + u0 + u1*age + u2*agesq

twoway ///
	(line yhat age if student==1) ///
	(line yhat age if student==2) ///
	(line yhat age if student==3) ///
	(line yhat age if student==4), legend(off)

twoway ///
	(line yhat age if student==10) ///
	(line yhat age if student==11) ///
	(line yhat age if student==12) ///
	(line yhat age if student==13) ///
	(line yhat age if student==14), legend(off)



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . . . .209



********************************************************************************
exit
