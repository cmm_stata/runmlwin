****************************************************************************
*     MLwiN User Manual 
*
* 8   Getting Started with your Data                                     107
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 8.1 Inputting your data set into MLwiN . . . . . . . . . . . . . . . . 107

*     Reading in an ASCII text data file . . . . . . . . . . . . . . . . 107

*     Common problems that can occur in reading ASCII data from
*     a text file . . . . . . . . . . . . . . . . . . . . . . . . . . . .108

*     Pasting data into a worksheet from the clipboard . . . . . . . . . 109

*     Naming columns . . . . . . . . . . . . . . . . . . . . . . . . . . 110

*     Adding category names . . . . . . . . . . . . . . . . . . . . . . .111

*     Missing data . . . . . . . . . . . . . . . . . . . . . . . . . . . 111

*     Unit identification columns . . . . . . . . . . . . . . . . . . . .112

*     Saving the worksheet . . . . . . . . . . . . . . . . . . . . . . . 112

*     Sorting your data set . . . . . . . . . . . . . . . . . . . . . . .112

* 8.2 Fitting models in MLwiN . . . . . . . . . . . . . . . . . . . . . .115

*     What are you trying to model? . . . . . . . . . . . . . . . . . . .115

*     Do you really need to fit a multilevel model? . . . . . . . . . . .115

*     Have you built up your model from a variance components model? . . 116

*     Have you centred your predictor variables? . . . . . . . . . . . . 116

*     Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . .116



****************************************************************************
exit