****************************************************************************
*     MLwiN User Manual 
*
* 17  Bootstrap Estimation                                               255
// George: Still need to get this chapter to work.
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 17.1 Introduction . . . . . . . . . . . . . . . . . . . . . . . . . . .255

* 17.2 Understanding the iterated bootstrap . . . . . . . . . . . . . . .256

* 17.3 An example of bootstrapping using MLwiN . . . . . . . . . . . . . 257

use "https://www.bristol.ac.uk/cmm/media/runmlwin/bes83.dta", clear

save "bes83.dta", replace

describe

runmlwin votecons cons defence unemp taxes privat, ///
	level2(area: cons) ///
	level1(voter:) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) ///
	rigls nopause


matrix a0 = e(b)

clear

set obs 1

generate set = 0

generate cons = a0[1,1]

generate defence = a0[1,2]

generate unemp = a0[1,3]

generate taxes = a0[1,4]

generate privat = a0[1,5]

generate sigma2u = a0[1,6]

save "original estimates.dta", replace





capture program drop bootstrapsim_inner

program define bootstrapsim_inner, rclass
	
	generate y = rbinomial(1, invlogit(xbu))

	runmlwin y cons defence unemp taxes privat, ///
		level2(area: cons, reset(none)) ///
		level1(voter:) ///
		discrete(distribution(binomial) link(logit) denominator(cons)) ///
		rigls maxiterations(25) plugin

	drop y

	return scalar cons       = [FP1]cons

	return scalar defence    = [FP1]defence

	return scalar unemp      = [FP1]unemp

	return scalar taxes      = [FP1]taxes

	return scalar privat     = [FP1]privat

	return scalar sigma2u    = [RP2]var(cons)

	return scalar iterations = e(iterations)

	return scalar converged  = e(converged)

	// George: Need to ignore the replicate if the replicate does not converged. Not sure how to do this.

end


capture program drop bootstrapsim_simulate_u

program define bootstrapsim_inner, rclass

		generate u = rnormal(0,sqrt(a`=`s'-1'[1,6]))

		bysort area (voter): replace u = u[1]

end


//capture program drop bootstrapsim_sample_u
//
//program define bootstrapsim_inner, rclass
//
//		generate u = rnormal(0,sqrt(a`=`s'-1'[1,6]))
//
//		bysort area (voter): replace u = u[1]
//
//end


capture prog drop bootstrapsim

prog define bootstrapsim

	syntax [, seed(integer 12345) sets(integer 5) replicates(integer 100) nonparametric]
	
	set seed `seed'
	
	forvalues s = 1/`sets' {

		use "bes83.dta", clear
		
		if ("`nonparametric'"=="") bootstrapsim_simulate_u

		if ("`nonparametric'"~="") bootstrapsim_sample_u

		generate xb = a`=`s'-1'[1,1] + a`=`s'-1'[1,2]*defence + a`=`s'-1'[1,3]*unemp + a`=`s'-1'[1,4]*taxes + a`=`s'-1'[1,5]*privat

		generate xbu = xb + u

		display  _n(3) as text "Replicate set: " as result "`s'" as text " of " as result "`sets'"

		simulate converged=r(converged) iterations=r(iterations) ///
			cons=r(cons) defence=r(defence) unemp=r(unemp) taxes=r(taxes) privat=r(privat) sigma2u=r(sigma2u), ///
			reps(`replicates'): bootstrapsim_inner

		generate set = `s'

		generate replicate =  _n

		order set replicate

// George: No bias correction has been made to these estimates

// George: need to add bias correction versions of the chains here
		
// George: need to add a raw chain running mean and a biase corrected running mean
		
		save "chains (set `s' of `sets') (`replicates' replicates per set).dta", replace 

		collapse (mean) set converged iterations cons defence unemp taxes privat sigma2u

		save "means (set `s' of `sets') (`replicates' replicates per set).dta", replace

		matrix a`s' = a`=`s'- 1'

		local i = 1

		foreach var of varlist cons defence unemp taxes privat sigma2u {

			quietly summarize `var'

			matrix a`s'[1,`i'] = a0[1,`i'] + (a`=`s'- 1'[1,`i'] - r(mean))

			local i = `i' + 1
		}

	}

	use "original estimates.dta", clear

	forvalues s = 1/`sets' {

		append using "means (set `s' of `sets') (`replicates' replicates per set).dta"

	}

	save "means (sets 1 to `sets') (`replicates' replicates per set).dta", replace

end


bootstrapsim, seed(12345) sets(5) replicates(100)





use "chains (set 1 of 5) (100 replicates per set).dta", clear

line sigma2u replicate in 1/60

quietly summarize sigma2u in 1/60

display "Estimate (no bias correction) = " r(mean)

generate sigma2u_bias_corrected = a0[1,6] + (a0[1,6] - sigma2u)

line sigma2u_bias_corrected replicate in 1/60

quietly summarize sigma2u_bias_corrected in 1/60

display "Bias corrected estimate = " r(mean)
// Note: We have not implemented the scaled SE's


use "means (sets 1 to 5) (100 replicates per set).dta", clear

twoway connected sigma2u set









bootstrapsim, seed(23489) sets(8) replicates(1000)

forvalues s = 0/8 {

	mat list a`s'

}

use "means (sets 1 to 8) (1000 replicates per set).dta", clear

twoway connected sigma2u set

use "chains (set 8 of 8) (1000 replicates per set).dta", clear

line sigma2u replicate


// We need to also scale the standard errors
// Graph of the running mean of the 5 bootstrap sets



* 17.4 Diagnostics and confidence intervals . . . . . . . . . . . . . . .263

use "chains (set 1 of 5) (100 replicates per set).dta", clear

kdensity sigma2u, xline(0)

summarize sigma2u



* 17.5 Nonparametric bootstrapping . . . . . . . . . . . . . . . . . . . 264

use "bes83.dta", clear

runmlwin votecons cons defence unemp taxes privat, ///
	level2(area: cons) ///
	level1(voter:) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) ///
	rigls nopause
	
generate xb = _b[cons]*cons + _b[defence]*defence + _b[unemp]*unemp + _b[taxes]*taxes + _b[privat]*privat

xtreg votecons, nocons offset(xb) mle i(area)




// Note. We have not attempted to replicate the nonparametric bootstrap
// illustrated in the manual. This is because the nonparameteric bootstrap
// samples unshrunken higher level residuals. There is no command in MLwiN
// to prodice unshrunken higher level residuals and so we can not import
// these into Stata from MLwiN.



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .270



****************************************************************************
exit