********************************************************************************
*     MLwiN User Manual 
*
* 9   Logistic Models for Binary and Binomial Responses                      117
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012). A User's 
*     Guide to MLwiN, v2.26. Centre for Multilevel Modelling, University of 
*     Bristol.
********************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
********************************************************************************

* 9.1 Introduction and description of the example data . . . . . . . . . . . 117

use "https://www.bristol.ac.uk/cmm/media/runmlwin/bang.dta", clear

describe


* 9.2 Single-level logistic regression . . . . . . . . . . . . . . . . . . . 119

* Link functions . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 119

* Interpretation of coeficients . . . . . . . . . . . . . . . . . . . . . . .120

* Fitting a single-level logit model in MLwiN . . . . . . . . . . . . . . . .121

tabulate lc use, row

generate lc1 = (lc==1)

generate lc2 = (lc==2)

generate lc3plus = (lc==3)

runmlwin use cons lc1 lc2 lc3plus, level1(woman) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) nopause
	
test [FP1]lc1 = [FP1]lc2



* A probit model . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 126

runmlwin use cons lc1 lc2 lc3plus, level1(woman) ///
	discrete(distribution(binomial) link(probit) denominator(cons)) nopause

runmlwin use cons lc1 lc2 lc3plus age, level1(woman) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) nopause



* 9.3 A two-level random intercept model . . . . . . . . . . . . . . . . . . 128

* Model specification . . . . . . . . . . . . . . . . . . . . . . . . . . . .128

* Estimation procedures . . . . . . . . . . . . . . . . . . . . . . . . . . .128

* Fitting a two-level random intercept model in MLwiN . . . . . . . . . . . .129

runmlwin use cons lc1 lc2 lc3plus age, ///
	level2(district: cons) level1(woman) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) nopause

runmlwin use cons lc1 lc2 lc3plus age, ///
	level2(district: cons) level1(woman) ///
	discrete(distribution(binomial) link(logit) denominator(cons) pql2) ///
	initsprevious ///
	nopause

test [RP2]var(cons)=0



* Variance partition coeficient . . . . . . . . . . . . . . . . . . . . . . .131

preserve

	set obs 5000

	set seed 12345

	generate u = sqrt([RP2]var(cons))*invnormal(uniform())

	generate p1 = invlogit(_b[cons] + u)

	generate p2 = invlogit(_b[cons] + _b[lc3plus] + _b[age]*-9.7 + u)

	generate p3 = invlogit(_b[cons] + _b[age]*15.3 + u)

	forvalues p = 1/3 {

		generate v`p' = p`p'*(1 - p`p')

		quietly summarize p`p'

		scalar lev2var`p' = r(sd)^2

		quietly summarize v`p'

		scalar lev1var`p' =  r(mean)

	}

	display "VPC = " lev2var1/(lev2var1 + lev1var1)

	display "VPC for a young women with 3+ children (low probability use) = " lev2var2/(lev2var2 + lev1var2)

	display "VPC for an old woman with no children (high probability use) = " lev2var3/(lev2var3 + lev1var3)

restore



* Adding further explanatory variables . . . . . . . . . . . . . . . . . . . 134

tabulate educ

generate ed_lprim = (educ==2)

generate ed_uprim = (educ==3)

generate ed_secplus = (educ==4)

runmlwin use cons lc1 lc2 lc3plus age urban ///
	ed_lprim ed_uprim ed_secplus hindu, ///
	level2(district: cons) level1(woman) ///
	discrete(distribution(binomial) link(logit) denominator(cons) pql2) ///
	initsprevious ///
	nopause



* 9.4 A two-level random coeficient model . . . . . . . . . . . . . . . . . .135

runmlwin use cons lc1 lc2 lc3plus age urban ///
	ed_lprim ed_uprim ed_secplus hindu, ///
	level2(district: cons urban) level1(woman) ///
	discrete(distribution(binomial) link(logit) denominator(cons) pql2) ///
	initsprevious ///
	nopause

test ([RP2]cov(cons\urban)=0) ([RP2]var(urban)=0), mtest

runmlwin use cons lc1 lc2 lc3plus age urban ///
	ed_lprim ed_uprim ed_secplus hindu d_lit d_pray, ///
	level2(district: cons urban) level1(woman) ///
	discrete(distribution(binomial) link(logit) denominator(cons) pql2) ///
	initsprevious ///
	nopause



* 9.5 Modelling binomial data . . . . . . . . . . . . . . . . . . . . . . . .139

* Modelling district-level variation with district-level proportions . . . . 139

* Creating a district-level data set . . . . . . . . . . . . . . . . . . . . 140

collapse (mean) use cons (sum) denom = cons, by(district d_lit d_pray)



* Fitting the model . . . . . . . . . . . . . . . . . . . . . . . . . . . . .142

runmlwin use cons d_lit d_pray, ///
	level2(district: cons) level1(district) ///
	discrete(distribution(binomial) link(logit) denominator(denom)) nopause

runmlwin use cons d_lit d_pray, ///
	level2(district: cons) level1(district) ///
	discrete(distribution(binomial) link(logit) denominator(denom) pql2) ///
	initsprevious ///
	nopause
// Note: The screenshot in the manual incorrectly dispalys the PQL1 estimates
// when it should show the PQL2 estimates.


	
* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . . . .143



****************************************************************************
exit
