****************************************************************************
*     MLwiN User Manual 
*
* 4   Random Intercept and Random Slope Models                            47
*
*     Rasbash, J., Steele, F., Browne, W. J. and Goldstein, H. (2012).
*     A User’s Guide to MLwiN, v2.26. Centre for Multilevel Modelling,
*     University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 4.1 Random intercept models . . . . . . . . . . . . . . . . . . . . . . 47

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

scatter normexam standlrt, ///
	yline(0) xline(0) ylabel(-4(1)4) xlabel(-4(1)4) aspectratio(1)

runmlwin normexam cons standlrt, level1(student: cons) nopause

runmlwin normexam cons standlrt, ///
	level2(school: cons, residuals(u)) level1(student: cons)  nopause



* 4.2 Graphing predicted school lines from a random intercept model . . . 51

predict xbu

line xbu standlrt

egen pickone = tag(school)

list u0 if pickone==1 & school<=6

replace xbu = xbu + u0

drop u0*

line xbu standlrt

sort school standlrt

line xbu standlrt, connect(a)

drop xbu



* 4.3 The effect of clustering on the standard errors of coeficients . . .58

generate boysch = (schgend==2)

generate girlsch = (schgend==3)

runmlwin normexam cons standlrt boysch girlsch, ///
	level2(school: cons) level1(student: cons) nopause

runmlwin normexam cons standlrt boysch girlsch, ///
	level1(student: cons) nopause



* 4.4 Does the coeficient of standlrt vary across schools? Introducing a 
*     random slope . . . . . . . . . . . . . . . . . . . . . . . . . . . .59

runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt, residuals(u)) ///
	level1(student: cons) nopause



* 4.5 Graphing predicted school lines from a random slope model . . . . . 62

predict xb

generate rphat = u0 + u1*standlrt

generate xbu = xb + rphat

sort school standlrt

line xbu standlrt, connect(a)



*     Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . 64



****************************************************************************
exit