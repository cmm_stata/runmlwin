****************************************************************************
*     MLwiN MCMC Manual 
*
* 18  Multivariate Normal Response Models and Missing Data . . . . . . . 263
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 18.1 GCSE science data with complete records only . . . . . . . . . . .264

use "https://www.bristol.ac.uk/cmm/media/runmlwin/gcsecomp1.dta", clear

describe

summarize written csework

corr written csework



* 18.2 Fitting single level multivariate models . . . . . . . . . . . . .265

runmlwin ///
	(written cons, eq(1)) ///
	(csework cons, eq(2)), ///
	level1(student: (cons, eq(1)) (cons, eq(2))) ///
	nosort nopause

runmlwin ///
	(written cons, eq(1)) ///
	(csework cons, eq(2)), ///
	level1(student: (cons, eq(1)) (cons, eq(2))) ///
	mcmc(on) initsprevious nosort nopause



* 18.3 Adding predictor variables . . . . . . . . . . . . . . . . . . . .270

quietly runmlwin ///
	(written cons female, eq(1)) ///
	(csework cons female, eq(2)), ///
	level1(student: (cons, eq(1)) (cons, eq(2))) ///
	nosort nopause

runmlwin ///
	(written cons female, eq(1)) ///
	(csework cons female, eq(2)), ///
	level1(student: (cons, eq(1)) (cons, eq(2))) ///
	mcmc(on) initsprevious nosort nopause



* 18.4 A multilevel multivariate model . . . . . . . . . . . . . . . . . 271

quietly runmlwin ///
	(written cons female, eq(1)) ///
	(csework cons female, eq(2)), ///
	level2(school: (cons, eq(1)) (cons, eq(2))) ///
	level1(student: (cons, eq(1)) (cons, eq(2))) ///
	nopause

runmlwin ///
	(written cons female, eq(1)) ///
	(csework cons female, eq(2)), ///
	level2(school: (cons, eq(1)) (cons, eq(2)), residuals(u)) ///
	level1(student: (cons, eq(1)) (cons, eq(2))) ///
	mcmc(on) initsprevious nopause

egen pickone = tag(school)

egen u0rank = rank(u0) if pickone==1

egen u1rank = rank(u1) if pickone==1

list school u0rank if inlist(u0rank,1,73)

list school u1rank if inlist(u1rank,1,73)

serrbar u0 u0se u0rank if pickone==1, scale(1.4) yline(0) ///
	addplot( ///
		(scatter u0 u0rank if pickone==1 & school==68137, mcolor(lime) msize(3)) ///
		(scatter u0 u0rank if pickone==1 & school==68201, mcolor(red) msize(3)) ///
		(scatter u0 u0rank if pickone==1 & school==68711, mcolor(pink) msize(3)) ///
		(scatter u0 u0rank if pickone==1 & school==60427, mcolor(cyan) msize(3)) ///
		(scatter u0 u0rank if pickone==1 & school==22710, mcolor(yellow) msize(3)) ///
		(scatter u0 u0rank if pickone==1 & school==67105, mcolor(gray) msize(3)) ///
	) legend(off)

serrbar u1 u1se u1rank if pickone==1, scale(1.4) yline(0) ///
	addplot( ///
		(scatter u1 u1rank if pickone==1 & school==68137, mcolor(lime) msize(3)) ///
		(scatter u1 u1rank if pickone==1 & school==68201, mcolor(red) msize(3)) ///
		(scatter u1 u1rank if pickone==1 & school==68711, mcolor(pink) msize(3)) ///
		(scatter u1 u1rank if pickone==1 & school==60427, mcolor(cyan) msize(3)) ///
		(scatter u0 u0rank if pickone==1 & school==22710, mcolor(yellow) msize(3)) ///
		(scatter u0 u0rank if pickone==1 & school==67105, mcolor(gray) msize(3)) ///
	) legend(off)

twoway ///
	(scatter u1 u0 if pickone==1) ///
	(scatter u1 u0 if pickone==1 & school==68137, mcolor(lime) msize(3)) ///
	(scatter u1 u0 if pickone==1 & school==68201, mcolor(red) msize(3)) ///
	(scatter u1 u0 if pickone==1 & school==68711, mcolor(pink) msize(3)) ///
	(scatter u1 u0 if pickone==1 & school==60427, mcolor(cyan) msize(3)) ///
	(scatter u1 u0 if pickone==1 & school==22710, mcolor(yellow) msize(3)) ///
	(scatter u1 u0 if pickone==1 & school==67105, mcolor(gray) msize(3)), ///
	yline(0) xline(0) aspectratio(1) legend(off)


* 18.5 GCSE science data with missing records . . . . . . . . . . . . . .275

use "https://www.bristol.ac.uk/cmm/media/runmlwin/gcsemv1.dta", clear

describe

quietly runmlwin ///
	(written cons female, eq(1)) ///
	(csework cons female, eq(2)), ///
	level1(student: (cons, eq(1)) (cons, eq(2))) ///
	nosort nopause

runmlwin (written cons female, eq(1)) ///
	(csework cons female, eq(2)), ///
	level1(student: (cons, eq(1)) (cons, eq(2))) ///
	mcmc(on) initsprevious nosort nopause

quietly runmlwin ///
	(written cons female, eq(1)) ///
	(csework cons female, eq(2)), ///
	level2(school: (cons, eq(1)) (cons, eq(2))) ///
	level1(student: (cons, eq(1)) (cons, eq(2))) ///
	nopause
	
runmlwin (written cons female, eq(1)) ///
	(csework cons female, eq(2)), ///
	level2(school: (cons, eq(1)) (cons, eq(2))) ///
	level1(student: (cons, eq(1)) (cons, eq(2))) ///
	mcmc(imputesummaries) initsprevious nopause 



* 18.6 Imputation methods for missing data . . . . . . . . . . . . . . . 280

* 18.7 Hungarian science exam dataset . . . . . . . . . . . . . . . . . .281

use "https://www.bristol.ac.uk/cmm/media/runmlwin/hungary1.dta", clear

describe

runmlwin ///
	(es_core   cons female, eq(1)) ///
	(biol_core cons female, eq(2)) ///
	(biol_r3   cons female, eq(3)) ///
	(biol_r4   cons female, eq(4)) ///
	(phys_core cons female, eq(5)) ///
	(phys_r2   cons female, eq(6)), ///
	level2(school: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
	) ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
	) ///
	nopause


runmlwin ///
	(es_core   cons female, eq(1)) ///
	(biol_core cons female, eq(2)) ///
	(biol_r3   cons female, eq(3)) ///
	(biol_r4   cons female, eq(4)) ///
	(phys_core cons female, eq(5)) ///
	(phys_r2   cons female, eq(6)), ///
	level2(school: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
	) ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
	) ///
	mcmc(imputeiterations(1000 2000 3000 4000 5000)) initsprevious nopause

format %4.3f es_core biol_core biol_r3 biol_r4 phys_core phys_r2

bysort _mi_m (_mi_id): list es_core biol_core biol_r3 biol_r4 phys_core phys_r2 if _n<=3



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .286





****************************************************************************
exit
