****************************************************************************
*     MLwiN MCMC Manual 
*
* 20  Multilevel Factor Analysis Modelling . . . . . . . . . . . . . . . 303
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 20.1 Factor analysis modelling . . . . . . . . . . . . . . . . . . . . 303

* 20.2 MCMC algorithm . . . . . . . . . . . . . . . . . . . . . . . . . .304

* 20.3 Hungarian science exam dataset . . . . . . . . . . . . . . . . . .304

use "https://www.bristol.ac.uk/cmm/media/runmlwin/hungary1.dta", clear

describe

summarize es_core biol_core phys_core

correlate es_core biol_core phys_core

describe

runmlwin ///
	(es_core   cons, eq(1)) ///
	(biol_core cons, eq(2)) ///
	(biol_r3   cons, eq(3)) ///
	(biol_r4   cons, eq(4)) ///
	(phys_core cons, eq(5)) ///
	(phys_r2   cons, eq(6)) ///
	, ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
	) ///
	nopause

runmlwin, correlation



* 20.4 A single factor Bayesian model . . . . . . . . . . . . . . . . . .307

runmlwin ///
	(es_core   cons, eq(1)) ///
	(biol_core cons, eq(2)) ///
	(biol_r3   cons, eq(3)) ///
	(biol_r4   cons, eq(4)) ///
	(phys_core cons, eq(5)) ///
	(phys_r2   cons, eq(6)) ///
	, ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)), diagonal ///
	) ///
	nopause

matrix flinit = (1\0\0\0\0\0)

matrix flconstr = (1\0\0\0\0\0)

matrix fvinit = (1)

matrix fvconstr = (0)
	
runmlwin ///
	(es_core   cons, eq(1)) ///
	(biol_core cons, eq(2)) ///
	(biol_r3   cons, eq(3)) ///
	(biol_r4   cons, eq(4)) ///
	(phys_core cons, eq(5)) ///
	(phys_r2   cons, eq(6)) ///
	, ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
		, diagonal flinits(flinit) flconstraints(flconstr) fvinits(fvinit) fvconstraints(fvconstr) fscores(f) ///
	) ///
	mcmc(on) initsprevious nopause

sort f1

generate f1rank = _n

scatter f1 f1rank

list student es_core biol_core biol_r3 biol_r4 phys_core phys_r2 f1 if f1rank==1, noobs

list student es_core biol_core biol_r3 biol_r4 phys_core phys_r2 f1 if f1rank==_N, noobs

drop f1*



* 20.5 Adding a second factor to the model . . . . . . . . . . . . . . . 313

sort school student

quietly runmlwin ///
	(es_core   cons, eq(1)) ///
	(biol_core cons, eq(2)) ///
	(biol_r3   cons, eq(3)) ///
	(biol_r4   cons, eq(4)) ///
	(phys_core cons, eq(5)) ///
	(phys_r2   cons, eq(6)) ///
	, ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
		, diagonal ///
	) ///
	nopause

matrix flinit = (1,0\0,1\0,0\0,0\0,0\0,0)

matrix flconstr = (1,1\0,1\0,0\0,0\0,0\0,0)

matrix fvinit = (1,.\.,1)

matrix fvconstr = (0,.\.,0)

runmlwin ///
	(es_core   cons, eq(1)) ///
	(biol_core cons, eq(2)) ///
	(biol_r3   cons, eq(3)) ///
	(biol_r4   cons, eq(4)) ///
	(phys_core cons, eq(5)) ///
	(phys_r2   cons, eq(6)) ///
	, ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
		, diagonal flinits(flinit) flconstraints(flconstr) fvinits(fvinit) fvconstraints(fvconstr) fscores(f) ///
	) ///
	mcmc(on) initsprevious nopause
	
scatter f1 f2

twoway ///
	(scatter f1 f2 if ~inlist(student,664,1572)) ///
	(scatter f1 f2 if student==664) ///
	(scatter f1 f2 if student==1572) ///
	, legend(order(2 "Student 664" 3 "Student 1572"))

sort f2

generate f2rank = _n

list student es_core biol_core biol_r3 biol_r4 phys_core phys_r2 f1 f2 f2rank if f2rank==1 | student==1572, noobs

drop f1* f2*



* 20.6 Examining the chains of the loading estimates . . . . . . . . . . 317

sort school student

mcmcsum RP1FL:f1_2, detail

mcmcsum RP1FL:f1_2, fiveway

mcmcsum RP1FL:f2_3, detail

mcmcsum RP1FL:f2_3, fiveway
	
quietly runmlwin ///
	(es_core   cons, eq(1)) ///
	(biol_core cons, eq(2)) ///
	(biol_r3   cons, eq(3)) ///
	(biol_r4   cons, eq(4)) ///
	(phys_core cons, eq(5)) ///
	(phys_r2   cons, eq(6)) ///
	, ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
		, diagonal ///
	) ///
	nopause	

runmlwin ///
	(es_core   cons, eq(1)) ///
	(biol_core cons, eq(2)) ///
	(biol_r3   cons, eq(3)) ///
	(biol_r4   cons, eq(4)) ///
	(phys_core cons, eq(5)) ///
	(phys_r2   cons, eq(6)) ///
	, ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
		, diagonal flinits(flinit) flconstraints(flconstr) fvinits(fvinit) fvconstraints(fvconstr) ///
	) ///
	mcmc(burnin(5000) chain(10000)) initsprevious nopause	

mcmcsum RP1FL:f2_3, detail

mcmcsum RP1FL:f2_3, fiveway	



* 20.7 Correlated factors . . . . . . . . . . . . . . . . . . . . . . . .319

quietly runmlwin ///
	(es_core   cons, eq(1)) ///
	(biol_core cons, eq(2)) ///
	(biol_r3   cons, eq(3)) ///
	(biol_r4   cons, eq(4)) ///
	(phys_core cons, eq(5)) ///
	(phys_r2   cons, eq(6)) ///
	, ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
		, diagonal ///
	) ///
	nopause

matrix flinit = (1,0\0,1\0,0\0,0\0,0\0,0)

matrix flconstr = (1,1\0,1\0,0\0,0\0,0\0,0)

matrix fvinit = (1,0\0,1)

matrix fvconstr = (0,0\0,0)

runmlwin ///
	(es_core   cons, eq(1)) ///
	(biol_core cons, eq(2)) ///
	(biol_r3   cons, eq(3)) ///
	(biol_r4   cons, eq(4)) ///
	(phys_core cons, eq(5)) ///
	(phys_r2   cons, eq(6)) ///
	, ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)), ///
		diagonal flinits(flinit) flconstraints(flconstr) fvinits(fvinit) fvconstraints(fvconstr) ///
	) ///
	mcmc(burnin(5000) chain(10000)) initsprevious nopause



* 20.8 Multilevel factor analysis . . . . . . . . . . . . . . . . . . . .320

sort school student

runmlwin ///
	(es_core   cons, eq(1)) ///
	(biol_core cons, eq(2)) ///
	(biol_r3   cons, eq(3)) ///
	(biol_r4   cons, eq(4)) ///
	(phys_core cons, eq(5)) ///
	(phys_r2   cons, eq(6)) ///
	, ///
	level2(school: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
	) ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
	) correlation ///
	nopause
	


* 20.9 Two level factor model . . . . . . . . . . . . . . . . . . . . . .321

quietly runmlwin ///
	(es_core   cons, eq(1)) ///
	(biol_core cons, eq(2)) ///
	(biol_r3   cons, eq(3)) ///
	(biol_r4   cons, eq(4)) ///
	(phys_core cons, eq(5)) ///
	(phys_r2   cons, eq(6)) ///
	, ///
	level2(school: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
		, diagonal ///
	) ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
		, diagonal ///
	) ///
	nopause

matrix flinit1 = (1\0\0\0\0\0)

matrix flconstr1 = (1\0\0\0\0\0)

matrix fvinit1 = (1)

matrix fvconstr1 = (0)

matrix flinit2 = (1\0\0\0\0\0)

matrix flconstr2 = (1\0\0\0\0\0)

matrix fvinit2 = (1)

matrix fvconstr2 = (0)	
	
runmlwin ///
	(es_core   cons, eq(1)) ///
	(biol_core cons, eq(2)) ///
	(biol_r3   cons, eq(3)) ///
	(biol_r4   cons, eq(4)) ///
	(phys_core cons, eq(5)) ///
	(phys_r2   cons, eq(6)) ///
	, ///
	level2(school: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
		, diagonal flinits(flinit2) flconstraints(flconstr2) fvinits(fvinit2) fvconstraints(fvconstr2) fscores(f) ///
	) ///
	level1(student: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
		(cons, eq(6)) ///
		, diagonal flinits(flinit1) flconstraints(flconstr1) fvinits(fvinit1) fvconstraints(fvconstr1) ///
	) ///
	mcmc(on) initsprevious nopause

keep school f1

duplicates drop

sort f1

generate f1rank = _n

scatter f1 f1rank

list if f1rank==1

list if f1rank==_N



* 20.10 Extensions and some warnings . . . . . . . . . . . . . . . . . . 324

* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .325





****************************************************************************
exit
