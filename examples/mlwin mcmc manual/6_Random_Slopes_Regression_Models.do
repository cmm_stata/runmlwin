****************************************************************************
*     MLwiN MCMC Manual 
*
* 6   Random Slopes Regression Models . . . . . . . . . . . . . . . . . . 71
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

tab school, gen(s)

forvalues s = 1/65 {

	gen s`s'Xstandlrt = s`s'*standlrt 

}

quietly runmlwin normexam cons standlrt s2-s65 s1Xstandlrt-s64Xstandlrt, ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons standlrt s2-s65 s1Xstandlrt-s64Xstandlrt, ///
	level1(student: cons) mcmc(on) initsprevious nopause

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	mcmc(on) initsprevious nopause
// Note: If you look at the MLwiN equations window, you will see that that
// the inverse wishart priors matrix S_u appears to be a zero matrix. This
// is a display problem in MLwiN. The values of S_u are, as they should be,
// the same as the initial values for the level 2 variance co-variance 
// matrix. This display problem applies to all MCMC models set up by 
// runmlwin which have two or more sets of random effects at a given level.



* 6.1 Prediction intervals for a random slopes regression model . . . . . 75

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt, residuals(u, savechains("u.dta", replace))) ///
	level1(student: cons) ///
	mcmc(chain(5001)) ///
	initsprevious nopause

putmata X=(cons standlrt)
putmata school_x=school
preserve

mcmcsum, getchains
putmata beta=(FP1_cons FP1_standlrt)

use "u.dta", clear
drop idnum
rename value u
reshape wide u, i(iteration school) j(residual)

sort school iteration

putmata residuals=(u0 u1)
putmata school_u=school
restore

ssc install moremata

mata
xb = X*beta'
grpnos = uniqrows(school_x);
for (i = 1; i < rows(grpnos); i++) {
	xind = selectindex(school_x :== i);
	uind = selectindex(school_u :== i);
	xgrp = X[xind, .];
	ugrp = residuals[uind, .];
	xu = xgrp*ugrp';
	xb[xind, .] = xb[xind, .] + xu;
}
quants = mm_quantile(xb', 1, (0.025 \ 0.5 \ 0.975))'

end

getmata (predlo predmd predhi)=quants

mata: mata drop X beta residuals school_x school_u grpnos xind uind xgrp ugrp xb xu i quants

sort school standlrt

twoway (line predmd standlrt, connect(a)) ///
	(line predlo standlrt, lcolor(maroon) lpattern(dot) connect(a)) ///
	(line predhi standlrt, lcolor(maroon) lpattern(dot) connect(a)), legend(off)

twoway (line predmd standlrt if school==30, lcolor(blue) connect(a)) ///
	(line predlo standlrt if school==30, lcolor(blue) lpattern(dot) connect(a)) ///
	(line predhi standlrt if school==30, lcolor(blue) lpattern(dot) connect(a)) ///
	(line predmd standlrt if school==44, lcolor(green) connect(a)) ///
	(line predlo standlrt if school==44, lcolor(green) lpattern(dot) connect(a)) ///
	(line predhi standlrt if school==44, lcolor(green) lpattern(dot) connect(a)) ///
	(line predmd standlrt if school==53, lcolor(ebblue) connect(a)) ///
	(line predlo standlrt if school==53, lcolor(ebblue) lpattern(dot) connect(a)) ///
	(line predhi standlrt if school==53, lcolor(ebblue) lpattern(dot) connect(a)) ///
	(line predmd standlrt if school==59, lcolor(maroon) connect(a)) ///
	(line predlo standlrt if school==59, lcolor(maroon) lpattern(dot) connect(a)) ///
	(line predhi standlrt if school==59, lcolor(maroon) lpattern(dot) connect(a)) ///
	, ytitle("Predicted age 16 exam score (normalised)") legend(order(1 "school_30" 4 "school_44" 7 "school_53" 10 "school_59"))


* 6.2 Alternative priors for variance matrices . . . . . . . . . . . . . .78

* 6.3 WinBUGS priors (Prior 2) . . . . . . . . . . . . . . . . . . . . . .78

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	nopause

estimates store IGLS

matrix b = e(b)

matrix V = e(V)

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	mcmc(on) initsb(b) initsv(V) nopause

estimates store GIBBS_default

matrix b2 = b

matrix list b2

matrix b2[1,3] = .1

matrix b2[1,4] = 0

matrix b2[1,5] = .1

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	mcmc(on) initsb(b2) initsv(V) nopause

estimates store GIBBS_prior_2



* 6.4 Uniform prior . . . . . . . . . . . . . . . . . . . . . . . . . . . 79

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	mcmc(rpprior(uniform)) initsb(b) initsv(V) nopause

estimates store GIBBS_uniform



* 6.5 Informative prior . . . . . . . . . . . . . . . . . . . . . . . . . 80

matrix P = (.*b \ .*b)

matrix rownames P = mean sd

matrix list P

matrix P[1,3] = .09

matrix P[1,4] = .018

matrix P[1,5] = .015

matrix P[2,3] = 65

matrix P[2,4] = 65

matrix P[2,5] = 65

matrix list P

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	mcmc(priormatrix(P)) initsb(b) initsv(V) nopause

estimates store GIBBS_prior_4



* 6.6 Results . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 81

estimates table IGLS ///
	GIBBS_default GIBBS_prior_2 GIBBS_uniform GIBBS_prior_4, b(%4.3f)


* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . . 81





****************************************************************************
exit
