****************************************************************************
*     MLwiN MCMC Manual 
*
* 16  Multiple Membership Models . . . . . . . . . . . . . . . . . . . . 231
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 16.1 Notation and weightings . . . . . . . . . . . . . . . . . . . . . 232

* 16.2 Office workers salary dataset . . . . . . . . . . . . . . . . . . 232

use "https://www.bristol.ac.uk/cmm/media/runmlwin/wage1.dta", clear

describe

histogram earnings

histogram logearn



* 16.3 Models for the earnings data . . . . . . . . . . . . . . . . . . .235

quietly runmlwin logearn cons age_40 numjobs, ///
	level2(company:) ///
	level1(id: cons) ///
	nopause

runmlwin logearn cons age_40 numjobs, ///
	level2(company:) ///
	level1(id: cons) ///
	mcmc(on) initsprevious ///
	nopause

quietly runmlwin logearn cons age_40 numjobs sex parttime, ///
	level2(company:) ///
	level1(id: cons) ///
	nopause

runmlwin logearn cons age_40 numjobs sex parttime, ///
	level2(company:) ///
	level1(id: cons) ///
	mcmc(on) initsprevious ///
	nopause

correlate parttime sex numjobs




* 16.4 Fitting multiple membership models to the dataset . . . . . . . . 237

tabulate numjobs

quietly runmlwin logearn cons age_40 sex parttime, ///
	level2(company: cons) ///
	level1(id: cons) ///
	nopause

matrix b = e(b)

matrix V = e(V)

runmlwin logearn cons age_40 sex parttime, ///
	level2(company: cons) ///
	level1(id: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause


runmlwin logearn cons age_40 sex parttime, ///
	level2(company: cons, mmids(company-company4) mmweights(weight1-weight4) residuals(u)) ///
	level1(id: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause



* 16.5 Residuals in multiple membership models . . . . . . . . . . . . . 240

preserve

	rename company company1

	keep company? id u0_? u0se_?

	reshape long company u0_ u0se_, i(id) j(order)

	drop id order

	rename u0_ u0
	
	rename u0se_ u0se

	drop if u0==.

	duplicates drop
	
	egen u0rank = rank(u0)

	serrbar u0 u0se u0rank, yline(0) scale(1.4)

restore

gen companyno54 = (company==54) + (company2==54) + (company3==54) + (company4==54)

gen companyno67 = (company==67) + (company2==67) + (company3==67) + (company4==67)

quietly runmlwin logearn cons age_40 sex parttime companyno54 companyno67, ///
	level2(company: cons) ///
	level1(id: cons) ///
	nopause

matrix b = e(b)

matrix V = e(V)

runmlwin logearn cons age_40 sex parttime companyno54 companyno67, ///
	level2(company: cons, mmids(company-company4) mmweights(weight1-weight4)) ///
	level1(id: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause



* 16.6 Alternative weights for multiple membership models . . . . . . . .243

runmlwin logearn cons age_40 sex parttime companyno54 companyno67, ///
	level2(company: cons, mmids(company-company4) mmweights(ew1-ew4)) ///
	level1(id: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause



* 16.7 Multiple membership multiple classification (MMMC) models . . . . 244

* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .245





****************************************************************************
exit
