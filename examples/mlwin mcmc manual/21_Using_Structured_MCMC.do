****************************************************************************
*     MLwiN MCMC Manual 
*
* 21  Using Structured MCMC . . . . . . . . . . . . . . . . . . . . . . .327
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 21.1 SMCMC Theory . . . . . . . . . . . . . . . . . . . . . . . . . . .327

* 21.2 Fitting the model using MLwiN . . . . . . . . . . . . . . . . . . 330

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

quietly runmlwin normexam cons, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause

matrix b = e(b)

matrix V = e(V)

runmlwin normexam cons, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause

mcmcsum [FP1]cons, fiveway

runmlwin normexam cons, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(smcmc) initsb(b) initsv(V) ///
	nopause

mcmcsum [FP1]cons, fiveway



* 21.3 A random intercepts model . . . . . . . . . . . . . . . . . . . . 334

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause
	
matrix b = e(b)

matrix V = e(V)

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(smcmc) initsb(b) initsv(V) ///
	nopause

mcmcsum, trajectories



* 21.4 Examining the residual chains . . . . . . . . . . . . . . . . . . 335

runmlwin normexam cons standlrt, ///
	level2(school: cons,  residuals(u, savechains("schoolresiduals.dta", replace))) ///
	level1(student: cons) ///
	mcmc(chain(5001) smcmc) initsb(b) initsv(V) ///
	nopause

use "schoolresiduals.dta", clear

keep if school==1

keep iteration value

mcmcsum value, variables detail

mcmcsum value, variables fiveway



* 21.5 Random slopes model theory . . . . . . . . . . . . . . . . . . . .336

* 21.6 Random Slopes model practice . . . . . . . . . . . . . . . . . . .338

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	nopause
	
runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	mcmc(smcmc) initsprevious ///
	nopause


mcmcsum [FP1]cons, fiveway

mcmcsum [FP1]standlrt, fiveway



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .340





****************************************************************************
exit
