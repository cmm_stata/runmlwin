****************************************************************************
*     MLwiN MCMC Manual 
*
* 2   Single Level Normal Response Modelling . . . . . . . . . . . . . . .21
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

describe

runmlwin normexam cons standlrt, ///
	level2(school:) ///
	level1(student: cons) ///
	nopause



* 2.1 Running the Gibbs Sampler . . . . . . . . . . . . . . . . . . . . . 26
runmlwin normexam cons standlrt, ///
	level2(school:) ///
	level1(student: cons) ///
	mcmc(on) initsprevious nopause 

mcmcsum, trajectories




* 2.2 Deviance statistic and the DIC diagnostic . . . . . . . . . . . . . 28


* 2.3 Adding more predictors . . . . . . . . . . . . . . . . . . . . . . .29

gen boysch = (schgend==2)

gen girlsch = (schgend==3)

runmlwin normexam cons standlrt girl boysch girlsch, ///
	level2(school:) ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons standlrt girl boysch girlsch, ///
	level2(school:) ///
	level1(student: cons) ///
	mcmc(on) initsprevious nopause




* 2.4 Fitting school effects as fixed parameters . . . . . . . . . . . . .32

tab school, gen(s)

quietly runmlwin normexam cons standlrt girl s2-s65, ///
	level2(school:) ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons standlrt girl s2-s65, ///
	level2(school:) ///
	level1(student: cons) ///
	mcmc(on) initsprevious nopause



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . . 33





****************************************************************************
exit
