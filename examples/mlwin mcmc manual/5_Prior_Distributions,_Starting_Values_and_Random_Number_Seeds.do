****************************************************************************
*     MLwiN MCMC Manual 
*
* 5   Prior Distributions, Starting Values and Random Number Seeds . . . .61
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 5.1 Prior distributions . . . . . . . . . . . . . . . . . . . . . . . . 61

* 5.2 Uniform on variance scale priors . . . . . . . . . . . . . . . . . .61

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause

estimates store IGLS

matrix b = e(b)

matrix V = e(V)

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause

estimates store GIBBS1

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(rppriors(uniform)) initsb(b) initsv(V) ///
	nopause

estimates store GIBBS2

estimates table IGLS GIBBS1 GIBBS2, b(%4.3f) se



* 5.3 Using informative priors . . . . . . . . . . . . . . . . . . . . . .62

matrix P = (.*b \ .*b)

matrix rownames P = mean sd

matrix list P

matrix P[1,2] = 1

matrix P[2,2] = .01

matrix list P

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(priormatrix(P)) initsb(b) initsv(V) ///
	nopause

matrix list P

matrix P[2,2] = .1

matrix list P

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(priormatrix(P)) initsb(b) initsv(V) ///
	nopause


* 5.4 Specifying an informative prior for a random parameter . . . . . . .65

matrix P = (.*b \ .*b)

matrix rownames P = mean sd

matrix list P

matrix P[1,3] = .2

matrix P[2,3] = 100

matrix list P

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(priormatrix(P)) initsb(b) initsv(V) ///
	nopause



* 5.5 Changing the random number seed and the parameter starting values  .66

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause

matrix b = e(b)

matrix V = e(V)

matrix b[1,1] = -2

matrix b[1,2] = 5

matrix b[1,3] = 2

matrix b[1,4] = 4

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(burnin(0) chain(500)) initsb(b) initsv(V) ///
	nopause

mcmcsum, trajectories

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause

matrix b = e(b)

matrix V = e(V)

forvalues s = 1/4 {

	quietly runmlwin normexam cons standlrt, ///
		level2(school: cons) ///
		level1(student: cons) ///
		mcmc(seed(`s')) initsb(b) initsv(V) ///
		nopause
	
	estimates store seed`s'
}

estimates table seed1 seed2 seed3 seed4, b(%4.3f)



* 5.6 Improving the speed of MCMC Estimation . . . . . . . . . . . . . . .69

* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . . 70





****************************************************************************
exit