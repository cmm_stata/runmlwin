****************************************************************************
*     MLwiN MCMC Manual 
*
* 23  Using Orthogonal fixed effect vectors . . . . . . . . . . . . . . .357
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 23.1 A simple example . . . . . . . . . . . . . . . . . . . . . . . . .358

* 23.2 Constructing orthogonal vectors . . . . . . . . . . . . . . . . . 359

* 23.3 A Binomial response example . . . . . . . . . . . . . . . . . . . 360

use "https://www.bristol.ac.uk/cmm/media/runmlwin/bang1.dta", clear

gen onekid = (lc==1)

gen twokids = (lc==2)

gen threepluskids = (lc==3)

quietly runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(denomb)) nopause

matrix b = e(b)

matrix V = e(V)

runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(denomb)) ///
	mcmc(on) initsb(b) initsv(V) nopause

mcmcsum, trajectories

runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(denomb)) ///
	mcmc(orth) initsb(b) initsv(V) nopause



* 23.4 A Poisson example . . . . . . . . . . . . . . . . . . . . . . . . 364

use "https://www.bristol.ac.uk/cmm/media/runmlwin/mmmec1.dta", clear

generate logexp = ln(exp)

tabulate nation

generate belgium     = (nation==1)

generate wgermany    = (nation==2)

generate denmark     = (nation==3)

generate france      = (nation==4)

generate uk          = (nation==5)

generate italy       = (nation==6)

generate ireland     = (nation==7)

generate luxembourg  = (nation==8)

generate netherlands = (nation==9)

foreach var of varlist belgium-netherlands {

	generate `var'Xuvbi = `var'*uvbi

}

quietly runmlwin obs belgium-netherlands belgiumXuvbi-netherlandsXuvbi, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) nopause

matrix b = e(b)

matrix V = e(V)

runmlwin obs belgium-netherlands belgiumXuvbi-netherlandsXuvbi, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) ///
	mcmc(chain(50000) seed(1)) initsb(b) initsv(V) nopause

mcmcsum [FP1]belgium, detail

mcmcsum [FP1]belgium, fiveway

runmlwin obs belgium-netherlands belgiumXuvbi-netherlandsXuvbi, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) ///
	mcmc(chain(50000) orth seed(1)) initsb(b) initsv(V) nopause
	
mcmcsum [FP1]belgium, detail

mcmcsum [FP1]belgium, fiveway
	
	

* 23.5 An Ordered multinomial example . . . . . . . . . . . . . . . . . .368

// With thanks to Erik who largely contributed this syntax

use "https://www.bristol.ac.uk/cmm/media/runmlwin/alevchem.dta", clear

describe

codebook, compact

generate gcseav = gcse_tot/gcse_no

histogram gcseav

replace gcseav = gcseav - 6

generate gcseav2 = gcseav^2

egen school = group(lea estab)
// Note: Establishment codes on their own do not uniquely identify schools.
// Schools are instead uniquely identified by LEA code, establishment ID 
// combination. Thus, here we generated a unique school ID.

rename gender female

runmlwin a_point cons (gcseav gcseav2 female, contrast(1/5)), ///
	level2(school: (cons, contrast(1/5))) ///
	level1(pupil:) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6) pql2) ///
	nopause 

matrix b = e(b)

matrix V = e(V)

runmlwin a_point cons (gcseav gcseav2 female, contrast(1/5)), ///
	level2(school: (cons, contrast(1/5))) ///
	level1(pupil:) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6)) ///
	mcmc(on) ///
	initsb(b) initsv(V) ///
	nopause

mcmcsum, trajectories

mcmcsum

runmlwin a_point cons (gcseav gcseav2 female, contrast(1/5)), ///
	level2(school: (cons, contrast(1/5))) ///
	level1(pupil:) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6)) ///
	mcmc(orth)  ///
	initsb(b) initsv(V) ///
	nopause

mcmcsum, trajectories

mcmcsum


* 23.6 The WinBUGS interface . . . . . . . . . . . . . . . . . . . . . . 372

use "https://www.bristol.ac.uk/cmm/media/runmlwin/bang1.dta", clear

gen onekid = (lc==1)

gen twokids = (lc==2)

gen threepluskids = (lc==3)

quietly runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(denomb)) nopause

matrix b = e(b)

matrix V = e(V)

runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(denomb)) ///
	mcmc(orth savewinbugs( ///
		model("orthog_model.txt", replace) ///
		inits("orthog_inits.txt", replace) ///
		data("orthog_data.txt", replace) ///
	))  ///
	initsb(b) initsv(V) ///
	nopause

wbscript , ///
	model("`c(pwd)'\orthog_model.txt") ///
	data("`c(pwd)'\orthog_data.txt") ///
	inits("`c(pwd)'\orthog_inits.txt") ///
	coda("`c(pwd)'\out") ///
	set(beta sigma2.u2) ///
	burn(500) update(5000) ///
	saving("`c(pwd)'\script.txt", replace) ///
	quit

wbrun, script("`c(pwd)'\script.txt") ///
	winbugs("C:\WinBUGS14\winbugs14.exe")

wbcoda, root("`c(pwd)'\out") clear

mcmcsum beta_1, fiveway variables

mcmcsum beta_1, detail variables

mcmcsum, variables

* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .379





****************************************************************************
exit
