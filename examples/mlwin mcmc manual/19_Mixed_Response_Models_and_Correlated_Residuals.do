****************************************************************************
*     MLwiN MCMC Manual 
*
* 19  Mixed Response Models and Correlated Residuals . . . . . . . . . . 287
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 19.1 Mixed response models . . . . . . . . . . . . . . . . . . . . . . 287

* 19.2 The JSP mixed response example . . . . . . . . . . . . . . . . . .289
use "https://www.bristol.ac.uk/cmm/media/runmlwin/jspmix1.dta", clear

describe

tabstat english, statistics(count mean) by(behaviour) columns(statistics)

corr sex fluent ravens english behaviour


* 19.3 Setting up a single level mixed response model . . . . . . . . . .291

runmlwin ///
	(english cons sex ravens fluent, eq(1)) ///
	(behaviour cons sex ravens, eq(2)), ///
	level1(id: (cons, eq(1))) ///
	discrete(distribution(normal binomial) link(probit) denom(cons denomb)) ///
	nosort nopause

runmlwin ///
	(english cons sex ravens fluent, eq(1)) ///
	(behaviour cons sex ravens, eq(2)), ///
	level1(id: (cons, eq(1))) ///
	discrete(distribution(normal binomial) link(probit) denom(cons denomb)) ///
	mcmc(on) initsprevious nosort nopause
// Note: MLwiN does not calculate the DIC for mixed response models and so
// the the DIC is not displayed in the runmlwin output. This issue applies
// to all the models in this chapter.



* 19.4 Multilevel mixed response model . . . . . . . . . . . . . . . . . 294
quietly runmlwin ///
	(english cons sex ravens fluent, eq(1)) ///
	(behaviour cons sex ravens, eq(2)), ///
	level2(school: (cons, eq(1)) (cons, eq(2))) ///
	level1(id: (cons, eq(1))) ///
	discrete(distribution(normal binomial) link(probit) denom(cons denomb)) ///
	nopause

runmlwin ///
	(english cons sex ravens fluent, eq(1)) ///
	(behaviour cons sex ravens, eq(2)), ///
	level2(school: (cons, eq(1)) (cons, eq(2))) ///
	level1(id: (cons, eq(1))) ///
	discrete(distribution(normal binomial) link(probit) denom(cons denomb)) ///
	mcmc(on) initsprevious nopause



* 19.5 Rats dataset . . . . . . . . . . . . . . . . . . . . . . . . . . .295

use "https://www.bristol.ac.uk/cmm/media/runmlwin/rats.dta", clear

describe

quietly runmlwin ///
	(y8  cons, eq(1)) ///
	(y15 cons, eq(2)) ///
	(y22 cons, eq(3)) ///
	(y29 cons, eq(4)) ///
	(y36 cons, eq(5)), ///
	level1(rat: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
	) ///
	nopause

matrix b = e(b)

matrix V = e(V)

runmlwin ///
	(y8  cons, eq(1)) ///
	(y15 cons, eq(2)) ///
	(y22 cons, eq(3)) ///
	(y29 cons, eq(4)) ///
	(y36 cons, eq(5)), ///
	level1(rat: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
	) ///
	mcmc(on) initsb(b) initsv(V) nopause

mcmcsum [RP1]var(cons_1), detail

mcmcsum [RP1]var(cons_1), fiveway



* 19.6 Fitting an autoregressive structure to the variance matrix . . . .298

runmlwin, corr
// Note: These correlations differ slightly from those presented in the
// manual. This is because those displayed in the manual are calculated
// based on the estimates of the variance and covariance parameters, while
// those presented here are based on full chains for the variance and
// covariance parameters. The method used to produce the correlations
// presented here is the more appropriate method. We would, however, 
// usually expect the two methods to give similar results.

runmlwin ///
	(y8  cons, eq(1)) ///
	(y15 cons, eq(2)) ///
	(y22 cons, eq(3)) ///
	(y29 cons, eq(4)) ///
	(y36 cons, eq(5)), ///
	level1(rat: ///
		(cons, eq(1)) ///
		(cons, eq(2)) ///
		(cons, eq(3)) ///
		(cons, eq(4)) ///
		(cons, eq(5)) ///
	) ///
	mcmc(chain(50000) refresh(500) corresiduals(arindepvars)) ///
	initsb(b) initsv(V) corr ///
	nopause


* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .301





****************************************************************************
exit
