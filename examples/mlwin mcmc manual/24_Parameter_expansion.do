****************************************************************************
*     MLwiN MCMC Manual 
*
* 24  Parameter expansion . . . . . . . . . . . . . . . . . . . . . . . .381
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 24.1 What is Parameter Expansion? . . . . . . . . . . . . . . . . . . .381

* 24.2 The tutorial example . . . . . . . . . . . . . . . . . . . . . . .383

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause
	
matrix b = e(b)

matrix V = e(V)

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause

mcmcsum [RP2]var(cons), detail

mcmcsum [RP2]var(cons), fiveway

runmlwin normexam cons standlrt, ///
	level2(school: cons, parexpansion) ///
	level1(student: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause

mcmcsum [RP2]var(cons), detail

mcmcsum [RP2]var(cons), fiveway



* 24.3 Binary responses - Voting example . . . . . . . . . . . . . . . . 386

use "https://www.bristol.ac.uk/cmm/media/runmlwin/bes83.dta", clear

runmlwin votecons cons defence unemp taxes privat, ///
	level2(area: cons) ///
	level1(voter:) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) ///
	nopause

matrix b = e(b)

matrix V = e(V)

runmlwin votecons cons defence unemp taxes privat, ///
	level2(area: cons) ///
	level1(voter:) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause

mcmcsum [RP2]var(cons), detail

mcmcsum [RP2]var(cons), fiveway
// Note: The ACF appears to decay far less rapidly than in the manual. This
// is simply because the y-axis min in the manual is not zero.

runmlwin votecons cons defence unemp taxes privat, ///
	level2(area: cons, parexpansion) ///
	level1(voter:) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause

mcmcsum [RP2]var(cons), detail

mcmcsum [RP2]var(cons), fiveway



* 24.4 The choice of prior distribution . . . . . . . . . . . . . . . . .390

runmlwin votecons cons defence unemp taxes privat, ///
	level2(area: cons) ///
	level1(voter:) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) ///
	mcmc(rppriors(uniform)) initsb(b) initsv(V) ///
	nopause

mcmcsum [RP2]var(cons), detail

mcmcsum [RP2]var(cons), fiveway



* 24.5 Parameter expansion and WinBUGS . . . . . . . . . . . . . . . . . 391

runmlwin votecons cons defence unemp taxes privat, ///
	level2(area: cons, parexpansion) ///
	level1(voter:) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) ///
	mcmc(savewinbugs( ///
		model("votecons_model.txt", replace) ///
		inits("votecons_inits.txt", replace) ///
		data("votecons_data.txt", replace) ///
	)) initsb(b) initsv(V) ///
	nopause

type "votecons_model.txt"

/* There is a known MLwiN bug here which will be fixed in version 2.29
wbscript, ///
	model("`c(pwd)'\votecons_model.txt") ///
	data("`c(pwd)'\votecons_data.txt") ///
	inits("`c(pwd)'\votecons_inits.txt") ///
	log("`c(pwd)'\votecons_log.txt") ///
	coda("`c(pwd)'\out") ///
	set(beta sigma2.v2 sigma2.u2 alpha2) ///
	burn(4000) update(5000) quit ///
	saving("`c(pwd)'\votecons_script.txt", replace)


wbrun, ///
	script("`c(pwd)'\votecons_script.txt") ///
	winbugs("C:\WinBUGS14\winbugs14.exe")

wbcoda, root("`c(pwd)'\out") clear

mcmcsum alpha2, variables

mcmcsum alpha2, variables fiveway

mcmcsum sigma2_v2, variables

mcmcsum sigma2_v2, variables fiveway
*/


* 24.6 Parameter expansion and random slopes . . . . . . . . . . . . . . 396

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	nopause
	
matrix b = e(b)

matrix V = e(V)

runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause


runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt, parexpansion) ///
	level1(student: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .399





****************************************************************************
exit