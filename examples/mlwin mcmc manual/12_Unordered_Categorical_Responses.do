****************************************************************************
*     MLwiN MCMC Manual 
*
* 12  Unordered Categorical Responses . . . . . . . . . . . . . . . . . .167
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

use "https://www.bristol.ac.uk/cmm/media/runmlwin/bang.dta", clear

describe

tabulate use4



* 12.1 Fitting a first single-level multinomial model . . . . . . . . . .169

label define use4 1 "ster" 2 "mod" 3 "trad" 4 "none", modify

label values use4 use4

quietly runmlwin use4 cons, ///
	level1(woman:) ///
	discrete(distribution(multinomial) link(mlogit) denominator(cons) basecategory(4)) ///
	nopause

runmlwin use4 cons, ///
	level1(woman:) ///
	discrete(distribution(multinomial) link(mlogit) denominator(cons) basecategory(4)) ///
	mcmc(on) initsprevious nopause

display "Pr(y = 1) = " exp([FP1]cons_1)/(1 + exp([FP1]cons_1) + exp([FP2]cons_2) + exp([FP3]cons_3))

display "Pr(y = 2) = " exp([FP2]cons_2)/(1 + exp([FP1]cons_1) + exp([FP2]cons_2) + exp([FP3]cons_3))

display "Pr(y = 3) = " exp([FP3]cons_3)/(1 + exp([FP1]cons_1) + exp([FP2]cons_2) + exp([FP3]cons_3))



* 12.2 Adding predictor variables . . . . . . . . . . . . . . . . . . . .173

generate lc1 = (lc==1)

generate lc2 = (lc==2)

generate lc3plus = (lc==3)

quietly runmlwin use4 cons lc1 lc2 lc3plus, ///
	level1(woman:) ///
	discrete(distribution(multinomial) link(mlogit) denominator(cons) basecategory(4)) ///
	nopause

runmlwin use4 cons lc1 lc2 lc3plus, ///
	level1(woman:) ///
	discrete(distribution(multinomial) link(mlogit) denominator(cons) basecategory(4)) ///
	mcmc(on) initsprevious nopause
// Note: These estinmates differ slightly from those presented in the
// manual. This is likely to be because the parameters have low ESS.

display "Pr(y = 3) = " exp([FP3]cons_3)/(1 + exp([FP1]cons_1) + exp([FP2]cons_2) + exp([FP3]cons_3))

display "Pr(y = 3) = " exp([FP3]cons_3 + [FP3]lc2_3)/( ///
		1 ///
		+ exp([FP1]cons_1 + [FP1]lc2_1) ///
		+ exp([FP2]cons_2 + [FP2]lc2_2) ///
		+ exp([FP3]cons_3 + [FP3]lc2_3) ///
	)



* 12.3 Interval estimates for conditional probabilities . . . . . . . . .175

preserve

	mcmcsum, getchains

	generate pred1 = exp(FP3_cons_3)/(1 + exp(FP1_cons_1) + exp(FP2_cons_2) + exp(FP3_cons_3))

	mcmcsum pred1, variables detail

	mcmcsum pred1, variables fiveway

	generate pred2 = exp(FP3_cons_3 + FP3_lc2_3)/( ///
			1 ///
			+ exp(FP1_cons_1 + FP1_lc2_1) ///
			+ exp(FP2_cons_2 + FP2_lc2_2) ///
			+ exp(FP3_cons_3 + FP3_lc2_3) ///
		)

	mcmcsum pred2, variables detail

	mcmcsum pred2, variables fiveway

restore



* 12.4 Adding district level random effects . . . . . . . . . . . . . . .177

runmlwin use4 cons lc1 lc2 lc3plus, ///
	level2(district: cons) ///
	level1(woman:) ///
	discrete(distribution(multinomial) link(mlogit) denominator(cons) basecategory(4) pql2) ///
	nopause

runmlwin use4 cons lc1 lc2 lc3plus, ///
	level2(district: cons) ///
	level1(woman:) ///
	discrete(distribution(multinomial) link(mlogit) denominator(cons) basecategory(4)) ///
	mcmc(on) initsprevious ///
	nopause

mcmcsum [RP2]var(cons_1), detail

mcmcsum [RP2]var(cons_1), fiveway

runmlwin, corr



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .180





****************************************************************************
exit
