****************************************************************************
*     MLwiN MCMC Manual 
*
* 1   Introduction to MCMC Estimation and Bayesian Modelling . . . . . . . 1
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     Chris Charlton and George Leckie, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 1.1 Bayesian modelling using Markov Chain Monte Carlo methods . . . . . .1

* 1.2 MCMC methods and Bayesian modelling . . . . . . . . . . . . . . . . .2

* 1.3 Default prior distributions . . . . . . . . . . . . . . . . . . . . .4

* 1.4 MCMC estimation . . . . . . . . . . . . . . . . . . . . . . . . . . .5

* 1.5 Gibbs sampling . . . . . . . . . . . . . . . . . . . . . . . . . . . 5

* 1.6 Metropolis Hastings sampling . . . . . . . . . . . . . . . . . . . . 8

* 1.7 Running macros to perform Gibbs sampling and Metropolis
*     Hastings sampling on the simple linear regression model . . . . . . 10

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

set seed 12345

generate y = normexam

generate x = standlrt

generate xsq = x^2

generate xy = x*y

foreach var of varlist y x xsq xy {

	quietly summarize `var'

	local sum`var' = r(sum)

}

quietly count

local N = r(N)

local beta0 = 0

local beta1 = 0

local sigma2e = 1

local epsilon = 0.001

local burnin = 500

local chain = 5000

local totaliterations = `burnin' + `chain'

local refresh = 50

if (`chain'>`N') set obs `chain'

quietly gen iteration = .

quietly gen beta0 = .

quietly gen beta1 = .

quietly gen sigma2e = .

quietly gen e2i = .

forvalues i = 1/`totaliterations' {

	local beta0 = (`sumy' - `beta1'*`sumx')/`N' + sqrt(`sigma2e'/`N')*invnormal(uniform())   

	local beta1 = (`sumxy' - `beta0'*`sumx')/`sumxsq' + sqrt(`sigma2e'/`sumxsq')*invnormal(uniform())

	quietly replace e2i = (y - (`beta0' + (`beta1'*x)))^2

	quietly summarize e2i

	local sume2i = r(sum)

	local sigma2e = 1/((1/(`epsilon' + (`sume2i'/2)))*(invgammap(`epsilon' + (`N'/2),uniform())))
	
	if (`i' > `burnin') {

		local c = `i' - `burnin'

		quietly replace iteration = `i' if _n==`c'

		quietly replace beta0 = `beta0' if _n==`c'
		
		quietly replace beta1 = `beta1' if _n==`c'
		
		quietly replace sigma2e = `sigma2e' if _n==`c'
	
	}
	
	if mod(`i',`refresh')==0 {
	
		display as text "Iteration `i'"
	
	}
	
}
	
tabstat beta0 beta1 sigma2e, stats(mean sd) format(%4.3f)



* 1.8 Dynamic traces for MCMC . . . . . . . . . . . . . . . . . . . . . . 12

line beta0 iteration

line beta1 iteration

line sigma2e iteration



* 1.9 Macro to run a hybrid Metropolis and Gibbs sampling method
*     for a linear regression example . . . . . . . . . . . . . . . . . . 15

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

set seed 12345

generate y = normexam

generate x = standlrt

generate xsq = x^2

generate xy = x*y

foreach var of varlist y x xsq xy {

	quietly summarize `var'

	local sum`var' = r(sum)

}

quietly count

local N = r(N)

local beta0 = 0

local beta1 = 0

local sigma2e = 1

local epsilon = 0.001

local burnin = 500

local chain = 5000

local beta0sd = 0.01

local beta1sd = 0.01

local beta0accept = 0

local beta1accept = 0

local totaliterations = `burnin' + `chain'

local refresh = 50

if (`chain'>`N') set obs `chain'

quietly gen iteration = .

quietly gen beta0 = .

quietly gen beta1 = .

quietly gen sigma2e = .

quietly gen e2i = .

forvalues i = 1/`totaliterations' {

	* Update beta0
	* Calculate new proposed beta0
	local beta0prop = `beta0' + `beta0sd'*invnormal(uniform())

	local beta0logpostdiff = -1*(2*(`beta0' - `beta0prop')*(`sumy' - `beta1'*`sumx') + `N'*((`beta0prop')^2 - (`beta0')^2))/(2*`sigma2e')

	if (`beta0logpostdiff' > 0) {

		* Definitely accept as higher posterior
		local beta0 = `beta0prop'

		local beta0accept = `beta0accept' + 1

	}

	else {

		* Only sometimes accept
		if (uniform() <  exp(`beta0logpostdiff')) {

			local beta0 = `beta0prop'

			local beta0accept = `beta0accept' + 1

		}

	}
	
	* Update beta1
	local beta1prop = `beta1' + `beta1sd'*invnormal(uniform())

	local beta1logpostdiff = -1*(2*(`beta1' - `beta1prop')*(`sumxy' - `beta0'*`sumx') + `sumxsq'*((`beta1prop')^2 - (`beta1')^2))/(2*`sigma2e')

	if (`beta1logpostdiff' > 0) {
	
		* Definitely accept as higher posterior
		local beta1 = `beta1prop'

		local beta1accept = `beta1accept' + 1

	}

	else {

		* Only sometimes accept
		if (uniform() < exp(`beta1logpostdiff')) {

			local beta1 = `beta1prop'

			local beta1accept = `beta1accept' + 1

		}

	}
	
	* Update sigma2e
	quietly replace e2i = (y - (`beta0' + (`beta1'*x)))^2

	quietly summarize e2i

	local sume2i = r(sum)

	local sigma2e = 1/((1/(`epsilon' + (`sume2i'/2)))*(invgammap(`epsilon' + (`N'/2),uniform())))
	
	if (`i' > `burnin') {

		local c = `i' - `burnin'

		quietly replace iteration = `i' if _n==`c'

		quietly replace beta0 = `beta0' if _n==`c'
		
		quietly replace beta1 = `beta1' if _n==`c'
		
		quietly replace sigma2e = `sigma2e' if _n==`c'
	
	}
	
	if mod(`i',`refresh')==0 {
	
		display as text "Iteration `i'"
	
	}
	
}
	
tabstat beta0 beta1 sigma2e, stats(mean sd) format(%4.3f)

display as text "beta0 proposal accepted `=`beta0accept'/5500' times"

display as text "beta1 proposal accepted `=`beta1accept'/5500' times"	



* 1.10 MCMC estimation of multilevel models in MLwiN . . . . . . . . . . .18

* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . . 19





****************************************************************************
exit
