****************************************************************************
*     MLwiN MCMC Manual 
*
* 11  Poisson Response Modelling . . . . . . . . . . . . . . . . . . . . 153
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

use "https://www.bristol.ac.uk/cmm/media/runmlwin/mmmec1.dta", clear

describe

generate logexp = ln(exp)



* 11.1 Simple Poisson regression model . . . . . . . . . . . . . . . . . 155

quietly runmlwin obs cons uvbi, ///
	level3(nation:) ///
	level2(region:) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) nopause

runmlwin obs cons uvbi, ///
	level3(nation:) ///
	level2(region:) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) ///
	mcmc(chain(50000) refresh(500)) initsprevious nopause

mcmcsum [FP1]uvbi, detail

mcmcsum [FP1]uvbi, fiveway




* 11.2 Adding in region level random effects . . . . . . . . . . . . . . 157

quietly runmlwin obs cons uvbi, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) nopause

runmlwin obs cons uvbi, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) ///
	mcmc(chain(50000)) initsprevious nopause

mcmcsum [FP1]uvbi, detail

mcmcsum [FP1]uvbi, fiveway



* 11.3 Including nation effects in the model . . . . . . . . . . . . . . 159

quietly runmlwin obs cons uvbi, ///
	level3(nation: cons) ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) nopause

runmlwin obs cons uvbi, ///
	level3(nation: cons) ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) ///
	mcmc(chain(50000)) initsprevious nopause

tabulate nation

generate belgium     = (nation==1)

generate wgermany    = (nation==2)

generate denmark     = (nation==3)

generate france      = (nation==4)

generate uk          = (nation==5)

generate italy       = (nation==6)

generate ireland     = (nation==7)

generate luxembourg  = (nation==8)

generate netherlands = (nation==9)

quietly runmlwin obs uvbi belgium-netherlands, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) nopause

runmlwin obs uvbi belgium-netherlands, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) ///
	mcmc(chain(50000)) initsprevious nopause



* 11.4 Interaction with UV exposure . . . . . . . . . . . . . . . . . . .161

foreach var of varlist belgium-netherlands {

	generate `var'Xuvbi = `var'*uvbi

}

quietly runmlwin obs belgium-netherlands belgiumXuvbi-netherlandsXuvbi, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) nopause

matrix b = e(b)

matrix V = e(V)

runmlwin obs belgium-netherlands belgiumXuvbi-netherlandsXuvbi, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) ///
	mcmc(chain(50000)) initsb(b) initsv(V) nopause



* 11.5 Problems with univariate updating Metropolis procedures . . . . . 163

mcmcsum [FP1]belgium, detail

mcmcsum [FP1]belgium, fiveway
// Note: The ACF appears to decay far less rapidly than in the manual. This
// is simply because the y-axis min in the manual is not zero.


runmlwin obs belgium-netherlands belgiumXuvbi-netherlandsXuvbi, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) ///
	mcmc(chain(500000) refresh(5000) thin(10)) initsb(b) initsv(V) nopause

mcmcsum [FP1]belgium, detail

mcmcsum [FP1]belgium, fiveway
// Note: The ACF appears to decay far less rapidly than in the manual. This
// is simply because the y-axis min in the manual is not zero.



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .166





****************************************************************************
exit