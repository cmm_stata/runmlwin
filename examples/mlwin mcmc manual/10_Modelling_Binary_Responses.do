****************************************************************************
*     MLwiN MCMC Manual 
*
* 10  Modelling Binary Responses . . . . . . . . . . . . . . . . . . . . 129
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

use "https://www.bristol.ac.uk/cmm/media/runmlwin/bang1.dta", clear

describe



* 10.1 Simple logistic regression model . . . . . . . . . . . . . . . . .130

quietly runmlwin use cons age, ///
	level2(district:) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) nopause

runmlwin use cons age, ///
	level2(district:) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) ///
	mcmc(on) initsprevious nopause level(90)

mcmcsum [FP1]age, detail

mcmcsum [FP1]age, fiveway

runmlwin use cons age, ///
	level2(district:) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) ///
	mcmc(chain(15000)) initsprevious nopause
// Note: Here runmlwin is fitting 15000 iterations from scratch. This
// contrasts the manual where we are fitting 10000 iterations in addition 
// to the original 5000 giving a total of 15000..

gen onekid = (lc==1)

gen twokids = (lc==2)

gen threepluskids = (lc==3)

quietly runmlwin use cons age onekid twokids threepluskids, ///
	level2(district:) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) nopause

runmlwin use cons age onekid twokids threepluskids, ///
	level2(district:) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) ///
	mcmc(on) initsprevious nopause



* 10.2 Random effects logistic regression model . . . . . . . . . . . . .136

runmlwin use cons age onekid twokids threepluskids, ///
	level2(district: cons) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) nopause

runmlwin use cons age onekid twokids threepluskids, ///
	level2(district: cons) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) ///
	mcmc(on) initsprevious nopause

mcmcsum [RP2]var(cons), detail

mcmcsum [RP2]var(cons), fiveway



* 10.3 Random coefficients for area type . . . . . . . . . . . . . . . . 139

quietly runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) nopause

runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) ///
	mcmc(on) initsprevious nopause

quietly runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) nopause

runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) ///
	mcmc(on) initsprevious nopause



* 10.4 Probit regression . . . . . . . . . . . . . . . . . . . . . . . . 141

* 10.5 Running a probit regression in MLwiN . . . . . . . . . . . . . . .142

quietly runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(probit) denom(cons)) nopause

matrix b = e(b)

matrix V = e(V)

runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(probit) denom(cons)) ///
	mcmc(on) initsb(b) initsv(V) nopause

estimates store mh

matrix mh_ess = e(ess)

runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(probit) denom(cons)) ///
	mcmc(femethod(gibbs) remethod(gibbs)) ///
	initsb(b) initsv(V) nopause

estimates store gibbs

matrix gibbs_ess = e(ess)

estimates table gibbs mh, b(%3.2f)

matrix ess = (gibbs_ess', mh_ess')

matrix list ess



* 10.6 Comparison with WinBUGS . . . . . . . . . . . . . . . . . . . . . 144

quietly runmlwin use cons age, ///
	level2(district: cons) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) nopause

matrix b = e(b)

matrix V = e(V)

quietly runmlwin use cons age, ///
	level2(district: cons) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) ///
	mcmc(savewinbugs( ///
		model("bang_model.txt", replace) ///
		inits("bang_inits.txt", replace) ///
		data("bang_data.txt", replace) ///
	nofit)) ///
	initsb(b) initsv(V) nopause

view "bang_model.txt"

/* There is a known MLwiN bug here which will be fixed in version 2.29
wbscript , ///
	model("`c(pwd)'\bang_model.txt") ///
	data("`c(pwd)'\bang_data.txt") ///
	inits("`c(pwd)'\bang_inits.txt") ///
	coda("`c(pwd)'\out") ///
	set(beta sigma2.u2) ///
	burn(4000) update(5000) ///
	saving("`c(pwd)'\script.txt", replace) ///
	quit

wbrun, script("`c(pwd)'\script.txt") ///
	winbugs("C:\WinBUGS14\winbugs14.exe")

wbcoda, root("`c(pwd)'\out") clear

mcmcsum beta_1, variables

mcmcsum beta_1, variables fiveway
*/

use "https://www.bristol.ac.uk/cmm/media/runmlwin/bang1.dta", clear

runmlwin use cons age, ///
	level2(district: cons) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(cons)) ///
	mcmc(on) initsb(b) initsv(V) nopause

mcmcsum [FP1]cons, detail

mcmcsum [FP1]cons, fiveway



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .151





****************************************************************************
exit
