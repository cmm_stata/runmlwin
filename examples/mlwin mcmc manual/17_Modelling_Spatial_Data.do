****************************************************************************
*     MLwiN MCMC Manual 
*
* 17  Modelling Spatial Data . . . . . . . . . . . . . . . . . . . . . . 247
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 17.1 Scottish lip cancer dataset . . . . . . . . . . . . . . . . . . . 247

use "https://www.bristol.ac.uk/cmm/media/runmlwin/lips1.dta", clear

describe



* 17.2 Fixed effects models . . . . . . . . . . . . . . . . . . . . . . .248

sort neigh1 area area

quietly runmlwin obs cons, ///
	level3(neigh1:) ///
	level2(area:) ///
	level1(area:) ///
	discrete(distribution(poisson) link(log) offset(offs)) ///
	nopause 

runmlwin obs cons, ///
	level3(neigh1:) ///
	level2(area:) ///
	level1(area:) ///
	discrete(distribution(poisson) link(log) offset(offs)) ///
	mcmc(on) initsprevious nopause


quietly runmlwin obs cons perc_aff, ///
	level3(neigh1:) ///
	level2(area:) ///
	level1(area:) ///
	discrete(distribution(poisson) link(log) offset(offs)) nopause 

runmlwin obs cons perc_aff, ///
	level3(neigh1:) ///
	level2(area:) ///
	level1(area:) ///
	discrete(distribution(poisson) link(log) offset(offs)) ///
	mcmc(on) initsprevious nopause



* 17.3 Random effects models . . . . . . . . . . . . . . . . . . . . . . 251

sort area

quietly runmlwin obs cons perc_aff, ///
	level3(neigh1:) ///
	level2(area: cons) ///
	level1(area:) ///
	discrete(distribution(poisson) link(log) offset(offs)) nopause

runmlwin obs cons perc_aff, ///
	level3(neigh1:) ///
	level2(area:cons) ///
	level1(area:) ///
	discrete(distribution(poisson) link(log) offset(offs)) ///
	mcmc(chain(50000) refresh(500)) initsprevious nopause



* 17.4 A spatial multiple-membership (MM) model . . . . . . . . . . . . .252

sort neigh1 area

quietly runmlwin obs cons perc_aff, ///
	level3(neigh1: cons) ///
	level2(area: cons) ///
	level1(area:) ///
	discrete(distribution(poisson) link(log) offset(offs)) nopause 

runmlwin obs cons perc_aff, ///
	level3(neigh1: cons, mmids(neigh1-neigh11) mmweights(weight1-weight11)) ///
	level2(area:cons) ///
	level1(area:) ///
	discrete(distribution(poisson) link(log) offset(offs)) ///
	mcmc(chain(50000) refresh(500)) initsprevious nopause



* 17.5 Other spatial models . . . . . . . . . . . . . . . . . . . . . . .255

* 17.6 Fitting a CAR model in MLwiN . . . . . . . . . . . . . . . . . . .255

sort area area area

quietly runmlwin obs perc_aff, ///
	level3(area: cons) ///
	level2(area:) ///
	level1(area:) ///
	discrete(distribution(poisson) link(log) offset(offs)) nopause 


runmlwin obs perc_aff, ///
	level3(area: cons, carids(neigh1-neigh11) carweights(wcar1-wcar11) residuals(v)) ///
	level2(area:) ///
	level1(area:) ///
	discrete(distribution(poisson) link(log) offset(offs)) ///
	mcmc(chain(50000) refresh(500) savewinbugs( ///
		model("car_model.txt", replace) /// 
		inits("car_inits.txt", replace) ///
		data("car_data.txt", replace) ///
	)) initsprevious nopause

preserve

	keep neigh? area v0_?

	reshape long neigh v0_, i(area) j(order)

	drop area order

	rename v0_ v0
	
	drop if v0==.

	duplicates drop
	
	sort v0
	
	sum v0
	
restore

/* There is a known MLwiN bug here which will be fixed in version 2.29
wbscript , ///
	model("`c(pwd)'\car_model.txt") ///
	data("`c(pwd)'\car_data.txt") ///
	inits("`c(pwd)'\car_inits.txt") ///
	coda("`c(pwd)'\out") ///
	set(beta_1 carmean sigma2_u3) ///
	burn(500) update(50000) ///
	saving("`c(pwd)'\script.txt", replace)
	

wbrun, script("`c(pwd)'\script.txt") ///
	winbugs("C:\WinBUGS14\winbugs14.exe")

wbcoda, root("`c(pwd)'\out") clear

mcmcsum beta_1 carmean sigma2_u3, variables

*/



* 17.7 Including exchangeable random effects . . . . . . . . . . . . . . 259

quietly runmlwin obs cons perc_aff, ///
	level3(area: cons) ///
	level2(area: cons) ///
	level1(area:) ///
	discrete(distribution(poisson) link(log) offset(offs)) nopause 

runmlwin obs cons perc_aff, ///
	level3(area: cons, carids(neigh1-neigh11) carweights(wcar1-wcar11)) ///
	level2(area: cons) ///
	level1(area:) ///
	discrete(distribution(poisson) link(log) offset(offs)) ///
	mcmc(chain(50000) refresh(500)) initsprevious nopause



* 17.8 Further reading on spatial modelling . . . . . . . . . . . . . . .260

* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .261





****************************************************************************
exit