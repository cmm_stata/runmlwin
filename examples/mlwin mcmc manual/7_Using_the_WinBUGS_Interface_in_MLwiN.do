****************************************************************************
*     MLwiN MCMC Manual 
*
* 7   Using the WinBUGS Interface in MLwiN . . . . . . . . . . . . . . . .83
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 7.1 Variance components models in WinBUGS . . . . . . . . . . . . . . . 84

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(savewinbugs( ///
		model("tutorial_model.txt", replace) ///
		inits("tutorial_inits.txt", replace) ///
		data("tutorial_data.txt", replace) ///
	)) ///
	initsprevious nopause

wbscript , ///
	model("`c(pwd)'\tutorial_model.txt") ///
	data("`c(pwd)'\tutorial_data.txt") ///
	inits("`c(pwd)'\tutorial_inits.txt") ///
	coda("`c(pwd)'\out") ///
	set(beta sigma2.u2 sigma2) ///
	burn(500) update(5000) ///
	saving("`c(pwd)'\script.txt", replace) ///
	quit

wbrun, script("`c(pwd)'\script.txt") ///
	winbugs("C:\WinBUGS14\winbugs14.exe")

wbcoda, root("`c(pwd)'\out") clear

mcmcsum beta_1 beta_2, variables

mcmcsum sigma2_u2 sigma2, variables

mcmcsum beta_2, fiveway variables




* 7.2 So why have a WinBUGS interface ? . . . . . . . . . . . . . . . . . 92

* 7.3 t distributed school residuals . . . . . . . . . . . . . . . . . . .92

view "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial1_model.txt"

view "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial1_inits.txt"

copy "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial1_model.txt" ///
	"tutorial1_model.txt", replace

copy "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial1_inits.txt" ///
	"tutorial1_inits.txt", replace

copy "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial1_data.txt" ///
	"tutorial1_data.txt", replace

wbscript , ///
	model("`c(pwd)'\tutorial1_model.txt") ///
	data("`c(pwd)'\tutorial1_data.txt") ///
	inits("`c(pwd)'\tutorial1_inits.txt") ///
	coda("`c(pwd)'\out") ///
	set(df) ///
	burn(500) update(5000) ///
	saving("`c(pwd)'\script.txt", replace) ///
	quit

wbrun, script("`c(pwd)'\script.txt") ///
	winbugs("C:\WinBUGS14\winbugs14.exe")

wbcoda, root("`c(pwd)'\out") clear

mcmcsum df, trajectories variables

mcmcsum df, variables

copy "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial2_model.txt" ///
	"tutorial2_model.txt", replace

copy "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial2_inits1.txt" ///
	"tutorial2_inits1.txt", replace

copy "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial2_inits2.txt" ///
	"tutorial2_inits2.txt", replace

copy "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial2_inits3.txt" ///
	"tutorial2_inits3.txt", replace

copy "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial2_data.txt" ///
	"tutorial2_data.txt", replace

wbscript , ///
	model("`c(pwd)'\tutorial2_model.txt") ///
	data("`c(pwd)'\tutorial2_data.txt") ///
	chains(3) ///
	inits("`c(pwd)'\tutorial2_inits.txt") ///
	coda("`c(pwd)'\out") ///
	set(df) ///
	burn(2000) update(10000) ///
	saving("`c(pwd)'\script.txt", replace) ///
	quit

wbrun, script("`c(pwd)'\script.txt") ///
	winbugs("C:\WinBUGS14\winbugs14.exe")

wbcoda, root("`c(pwd)'\out") id(chain) chains(3) multichain clear

twoway ///
	(line df order if chain==1) ///
	(line df order if chain==2) ///
	(line df order if chain==3), legend(off)

mcmcsum df, variables

copy "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial3_model.txt" ///
	"tutorial3_model.txt", replace

copy "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial3_inits.txt" ///
	"tutorial3_inits.txt", replace

copy "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial3_data.txt" ///
	"tutorial3_data.txt", replace

wbscript , ///
	model("`c(pwd)'\tutorial3_model.txt") ///
	data("`c(pwd)'\tutorial3_data.txt") ///
	inits("`c(pwd)'\tutorial3_inits.txt") ///
	coda("`c(pwd)'\out") ///
	set(beta sigma2.u2 sigma2) ///
	burn(500) update(5000) ///
	saving("`c(pwd)'\script.txt", replace) ///
	quit

wbrun, script("`c(pwd)'\script.txt") ///
	winbugs("C:\WinBUGS14\winbugs14.exe")

wbcoda, root("`c(pwd)'\out") clear

mcmcsum beta_1 beta_2 sigma2_u2 sigma2, variables



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . . 96





****************************************************************************
exit