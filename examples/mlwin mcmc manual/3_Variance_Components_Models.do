****************************************************************************
*     MLwiN MCMC Manual 
*
* 3   Variance Components Models . . . . . . . . . . . . . . . . . . . . .35
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 3.1 A 2 level variance components model for the Tutorial dataset . . . .36

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(on) initsprevious nopause

mcmcsum in 4501/5000, trajectories

mcmcsum [FP1]standlrt, detail

mcmcsum [FP1]standlrt, fiveway

mcmcsum [RP2]var(cons), detail

mcmcsum [RP2]var(cons), fiveway



* 3.2 DIC and multilevel models . . . . . . . . . . . . . . . . . . . . . 41



* 3.3 Comparison between fixed and random school effects . . . . . . . . .41

quietly runmlwin normexam cons standlrt girl, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons standlrt girl, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(on) initsprevious nopause



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . . 43





****************************************************************************
exit
