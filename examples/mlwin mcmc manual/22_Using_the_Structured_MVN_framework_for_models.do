****************************************************************************
*     MLwiN MCMC Manual 
*
* 22  Using the Structured MVN framework for models . . . . . . . . . . .341
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 22.1 MCMC theory for Structured MVN models . . . . . . . . . . . . . . 341

* 22.2 Using the SMVN framework in practice . . . . . . . . . . . . . . .344

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

runmlwin normexam cons, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause
	
matrix b = e(b)

matrix V = e(V)

runmlwin normexam cons, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause

runmlwin normexam cons, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(smvn) initsb(b) initsv(V) ///
	nopause
	
	

* 22.3 Model Comparison and structured MVN models . . . . . . . . . . . .349

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause
	
matrix b = e(b)

matrix V = e(V)

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause


runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(smcmc) initsb(b) initsv(V) ///
	nopause

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(smvn) initsb(b) initsv(V) ///
	nopause



* 22.4 Assessing the need for the level 2 variance . . . . . . . . . . . 350

set seed 12345

gen temp = invnorm(uniform())

runmlwin temp cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause
// Note: We have called this variable temp instead of c11 as calling it c11
// results in an error in MLwiN as c1-c1500 are column names in MLwiN.

runmlwin temp cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(smvn) initsprevious ///
	nopause
	
mcmcsum [RP2]var(cons), detail

mcmcsum [RP2]var(cons), fiveway



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .355





****************************************************************************
exit