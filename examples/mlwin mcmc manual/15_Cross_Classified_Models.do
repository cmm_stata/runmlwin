****************************************************************************
*     MLwiN MCMC Manual 
*
* 15  Cross Classified Models . . . . . . . . . . . . . . . . . . . . . .215
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 15.1 Classifications and levels . . . . . . . . . . . . . . . . . . . .216

* 15.2 Notation . . . . . . . . . . . . . . . . . . . . . . . . . . . . .217

* 15.3 The Fife educational dataset . . . . . . . . . . . . . . . . . . .217

use "https://www.bristol.ac.uk/cmm/media/runmlwin/xc1.dta", clear

describe

list attain pid sid pupil in 1/10

runmlwin attain cons, ///
	level3(sid: cons) level2(pid:) ///
	level1(pupil: cons) ///
	nopause

runmlwin attain cons, ///
	level3(sid: cons) level2(pid:) ///
	level1(pupil: cons) ///
	mcmc(on) initsprevious nopause



* 15.4 A Cross-classified model . . . . . . . . . . . . . . . . . . . . .220

runmlwin attain cons, ///
	level3(sid: cons) level2(pid: cons) ///
	level1(pupil: cons) ///
	nopause

runmlwin attain cons, ///
	level3(sid: cons, residuals(v)) ///
	level2(pid: cons, residuals(u)) ///
	level1(pupil: cons) ///
	mcmc(cc on) initsprevious nopause

mcmcsum [RP3]var(cons),  detail



* 15.5 Residuals . . . . . . . . . . . . . . . . . . . . . . . . . . . . 223

egen pickone = tag(sid)

egen v0rank = rank(v0) if pickone==1

serrbar v0 v0se v0rank if pickone==1, scale(0) yline(0)

list sid v0 if v0rank==1 & pickone==1

drop pickone

egen pickone = tag(pid)

egen u0rank = rank(u0) if pickone==1

serrbar u0 u0se u0rank if pickone==1, scale(0) yline(0)

list pid u0 if u0rank==1 & pickone==1

drop v0* u0* pickone



* 15.6 Adding predictors to the model . . . . . . . . . . . . . . . . . .225

use "https://www.bristol.ac.uk/cmm/media/runmlwin/xc1.dta", clear

runmlwin attain cons vrq, ///
	level3(sid: cons) level2(pid: cons) ///
	level1(pupil: cons) ///
	nopause

runmlwin attain cons vrq, ///
	level3(sid: cons) level2(pid: cons) ///
	level1(pupil: cons) ///
	mcmc(cc) initsprevious nopause

runmlwin attain cons vrq sc fed med choice, ///
	level3(sid: cons) level2(pid: cons) ///
	level1(pupil: cons) ///
	nopause

runmlwin attain cons vrq  sc fed med choice, ///
	level3(sid: cons, residuals(v)) level2(pid: cons) ///
	level1(pupil: cons) ///
	mcmc(cc) initsprevious nopause

egen pickone = tag(sid)

egen v0rank = rank(v0) if pickone==1

serrbar v0 v0se v0rank if pickone==1, scale(0) yline(0)

list sid v0 if v0rank==1 & pickone==1

gen school19 = (sid==19)

sort pid pupil

runmlwin attain cons vrq sc fed med choice school19, ///
	level2(pid: cons) ///
	level1(pupil: cons) ///
	nopause

runmlwin attain cons vrq  sc fed med choice school19, ///
	level2(pid: cons) ///
	level1(pupil: cons) ///
	mcmc(on) initsprevious nopause



* 15.7 Current restrictions for cross-classified models . . . . . . . . .229

* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .230





****************************************************************************
exit