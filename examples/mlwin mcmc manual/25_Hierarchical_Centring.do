****************************************************************************
*     MLwiN MCMC Manual 
*
* 25  Hierarchical Centring . . . . . . . . . . . . . . . . . . . . . . .401
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 25.1 What is hierarchical centering? . . . . . . . . . . . . . . . . . 401

* 25.2 Centring Normal models using WinBUGS . . . . . . . . . . . . . . .403

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(hcen(2) savewinbugs( ///
		model("m.txt", replace) /// 
		inits("i.txt", replace) ///
		data("d.txt", replace) ///
	)) initsprevious nopause



* 25.3 Binomial hierarchical centering algorithm . . . . . . . . . . . . 408

* 25.4 Binomial example in practice . . . . . . . . . . . . . . . . . . .410

use "https://www.bristol.ac.uk/cmm/media/runmlwin/bang1.dta", clear

gen onekid = (lc==1)

gen twokids = (lc==2)

gen threepluskids = (lc==3)

runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(denomb)) nopause

matrix b = e(b)

matrix V = e(V)

runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(denomb)) ///
	mcmc(hcen(2) seed(1)) initsb(b) initsv(V) nopause
	
mcmcsum, trajectories	

runmlwin use cons age onekid twokids threepluskids urban, ///
	level2(district: cons urban) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denom(denomb)) ///
	mcmc(orth hcen(2) seed(1)) initsb(b) initsv(V) nopause

mcmcsum, trajectories	



* 25.5 The Melanoma example . . . . . . . . . . . . . . . . . . . . . . .414

use "https://www.bristol.ac.uk/cmm/media/runmlwin/mmmec1.dta", clear

generate logexp = ln(exp)

tabulate nation

generate belgium     = (nation==1)

generate wgermany    = (nation==2)

generate denmark     = (nation==3)

generate france      = (nation==4)

generate uk          = (nation==5)

generate italy       = (nation==6)

generate ireland     = (nation==7)

generate luxembourg  = (nation==8)

generate netherlands = (nation==9)

foreach var of varlist belgium-netherlands {

	generate `var'Xuvbi = `var'*uvbi

}

quietly runmlwin obs belgium-netherlands belgiumXuvbi-netherlandsXuvbi, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) nopause

matrix b = e(b)

matrix V = e(V)

runmlwin obs belgium-netherlands belgiumXuvbi-netherlandsXuvbi, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) ///
	mcmc(chain(50000) hcen(2) seed(1)) initsb(b) initsv(V) nopause

mcmcsum [FP1]belgium, detail

runmlwin obs belgium-netherlands belgiumXuvbi-netherlandsXuvbi, ///
	level2(region: cons) ///
	level1(county:) ///
	discrete(distribution(poisson) offset(logexp)) ///
	mcmc(chain(50000) orth hcen(2) seed(1)) initsb(b) initsv(V) nopause

mcmcsum [FP1]belgium, detail



* 25.6 Normal response models in MLwiN . . . . . . . . . . . . . . . . . 419

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	nopause

matrix b = e(b)

matrix V = e(V)

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(femethod(univariatemh) remethod(univariatemh) hcen(2) seed(1)) initsb(b) initsv(V) nopause

mcmcsum, trajectories	

runmlwin normexam cons standlrt, ///
	level2(school: cons) ///
	level1(student: cons) ///
	mcmc(hcen(2) seed(1)) initsb(b) initsv(V) nopause

mcmcsum, trajectories	


* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .422





****************************************************************************
exit