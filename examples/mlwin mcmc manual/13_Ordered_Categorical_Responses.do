****************************************************************************
*     MLwiN MCMC Manual 
*
* 13  Ordered Categorical Responses . . . . . . . . . . . . . . . . . . .181
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 13.1 A level chemistry dataset . . . . . . . . . . . . . . . . . . . . 181

use "https://www.bristol.ac.uk/cmm/media/runmlwin/alevchem.dta", clear

describe

generate gcseav = gcse_tot/gcse_no

histogram gcseav

replace gcseav = gcseav - 6

generate gcseav2 = gcseav^2

generate gcseav3 = gcseav^3



* 13.2 Normal response models . . . . . . . . . . . . . . . . . . . . . .184

quietly runmlwin a_point cons, ///
	level1(pupil: cons) ///
	nopause

runmlwin a_point cons, ///
	level1(pupil: cons) ///
	mcmc(on) initsprevious nopause

rename gender female

runmlwin a_point cons gcseav gcseav2 gcseav3 female, ///
	level1(pupil: cons)  nopause

runmlwin a_point cons gcseav gcseav2 gcseav3 female, ///
	level1(pupil: cons)  ///
	mcmc(on) initsprevious nopause

gen pred = [FP1]cons + [FP1]gcseav*gcseav + [FP1]gcseav2*gcseav2 ///
	+ [FP1]gcseav3*gcseav3 + [FP1]female*female

twoway ///
	(line pred gcseav if female==0, sort) ///
	(line pred gcseav if female==1, sort)




* 13.3 Ordered multinomial modelling . . . . . . . . . . . . . . . . . . 186

runmlwin a_point cons, ///
	level1(pupil:) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6)) nopause

runmlwin a_point cons, ///
	level1(pupil:) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6)) ///
	mcmc(on) initsprevious nopause

	

* 13.4 Adding predictor variables . . . . . . . . . . . . . . . . . . . .191

runmlwin a_point cons (gcseav gcseav2 gcseav3 female, contrast(1/5)), ///
	level1(pupil:) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6)) nopause

runmlwin a_point cons (gcseav gcseav2 gcseav3 female, contrast(1/5)), ///
	level1(pupil:) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6)) ///
	mcmc(on) initsprevious nopause



* 13.5 Multilevel ordered response modelling . . . . . . . . . . . . . . 192

egen school = group(lea estab)
// Note: Establishment codes on their own do not uniquely identify schools.
// Schools are instead uniquely identified by LEA code, establishment ID 
// combination. Thus, here we generated a unique school ID.

runmlwin a_point cons (gcseav gcseav2 female, contrast(1/5)), ///
	level2(school: (cons, contrast(1/5))) ///
	level1(pupil:) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6) pql2) nopause

runmlwin a_point cons (gcseav gcseav2 female, contrast(1/5)), ///
	level2(school: (cons, contrast(1/5))) ///
	level1(pupil:) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6)) ///
	mcmc(on) initsprevious nopause

runmlwin a_point cons (gcseav gcseav2 female, contrast(1/5)), ///
	level2(school: (cons gcseav, contrast(1/5))) ///
	level1(pupil:) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6)) nopause

matrix b = e(b)

matrix V = e(V)

runmlwin a_point cons (gcseav gcseav2 female, contrast(1/5)), ///
	level2(school: (cons gcseav, contrast(1/5))) ///
	level1(pupil:) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6)) ///
	mcmc(on) initsb(b) initsv(V) nopause

mcmcsum [RP2]var(cons_12345), detail

mcmcsum [RP2]var(cons_12345), fiveway

runmlwin a_point cons (gcseav gcseav2 female, contrast(1/5)), ///
	level2(school: (cons gcseav, contrast(1/5))) ///
	level1(pupil:) ///
	discrete(distribution(multinomial) link(ologit) denominator(cons) basecategory(6)) ///
	mcmc(chain(50000)) initsb(b) initsv(V) nopause

mcmcsum [RP2]var(cons_12345), detail

mcmcsum [RP2]var(cons_12345), fiveway



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .196





****************************************************************************
exit
