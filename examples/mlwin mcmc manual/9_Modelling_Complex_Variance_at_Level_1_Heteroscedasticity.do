********************************************************************************
*     MLwiN MCMC Manual 
*
* 9   Modelling Complex Variance at Level 1 / Heteroscedasticity. . . . . . .111
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
********************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
********************************************************************************

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

tabstat normexam, by(girl) stats(N mean sd)

gen intakecat = standlrt

recode intakecat (-4/-1=0) (-1/-.5=1) (-.5/-.1=2) (-.1/.3=3) (.3/.7=4) ///
	(.7/1.1=5) (1.1/4=6)

tabstat normexam, by(intakecat) stats(N mean sd)



* 9.1 MCMC algorithm for a 1 level Normal model with complex variation . . . 113

* 9.2 Setting up the model in MLwiN . . . . . . . . . . . . . . . . . . . . .115

quietly runmlwin normexam cons standlrt, ///
	level1(student: cons standlrt) ///
	nopause

runmlwin normexam cons standlrt, ///
	level1(student: cons standlrt) ///
	mcmc(on) initsprevious nopause

mcmcsum in 4501/5000, trajectories

generate l1varfn = [RP1]var(cons) + 2*[RP1]cov(cons\standlrt)*standlrt ///
	+ [RP1]var(standlrt)*standlrt^2

line l1varfn standlrt, sort



* 9.3 Complex variance functions in multilevel models . . . . . . . . . . . .119

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	mcmc(on) initsprevious nopause

generate l2varfn = [RP2]var(cons) + 2*[RP2]cov(cons\standlrt)*standlrt ///
	+ [RP2]var(standlrt)*standlrt^2

replace l1varfn = [RP1]var(cons)

line l2varfn l1varfn standlrt, sort
	
matrix a = (1,1,0)

quietly runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons standlrt, elements(a)) ///
	nopause

runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons standlrt, elements(a)) ///
	mcmc(on) initsprevious nopause



* 9.4 Relationship with gender . . . . . . . . . . . . . . . . . . . . . . . 123

matrix a = (1,1,0,1,1,0)

runmlwin normexam cons standlrt girl, ///
	level2(school: cons standlrt) ///
	level1(student: cons standlrt girl, elements(a)) ///
	nopause

runmlwin normexam cons standlrt girl, ///
	level2(school: cons standlrt) ///
	level1(student: cons standlrt girl, elements(a)) ///
	mcmc(on) initsprevious nopause

replace l2varfn = [RP2]var(cons) + 2*[RP2]cov(cons\standlrt)*standlrt ///
	+ [RP2]var(standlrt)*standlrt^2

generate l1varfnboys = [RP1]var(cons) + 2*[RP1]cov(cons\standlrt)*standlrt

generate l1varfngirls = [RP1]var(cons) ///
	+ 2*[RP1]cov(cons\standlrt)*standlrt ///
	+ 2*[RP1]cov(cons\girl) + 2*[RP1]cov(standlrt\girl)*standlrt

line l2varfn l1varfnboys l1varfngirls standlrt, sort



* 9.5 Alternative log precision formulation . . . . . . . . . . . . . . . . .126

matrix a = (1,1,0,1,1,0)

quietly runmlwin normexam cons standlrt girl, ///
	level2(school: cons standlrt) ///
	level1(student: cons standlrt girl, elements(a)) ///
	nopause

runmlwin normexam cons standlrt girl, ///
	level2(school: cons standlrt) ///
	level1(student: cons standlrt girl, elements(a)) ///
	mcmc(log) initsprevious nopause

replace l2varfn = [RP2]var(cons) + 2*[RP2]cov(cons\standlrt)*standlrt ///
	+ [RP2]var(standlrt)*standlrt^2

replace l1varfnboys = [RP1]var(cons) + 2*[RP1]cov(cons\standlrt)*standlrt

replace l1varfngirls = [RP1]var(cons) ///
	+ 2*[RP1]cov(cons\standlrt)*standlrt ///
	+ 2*[RP1]cov(cons\girl) + 2*[RP1]cov(standlrt\girl)*standlrt


replace l1varfnboys = 1/exp(l1varfnboys)

replace l1varfngirls = 1/exp(l1varfngirls)


line l2varfn l1varfnboys l1varfngirls standlrt, sort



* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . . . .128





********************************************************************************
exit
