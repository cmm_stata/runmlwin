****************************************************************************
*     MLwiN MCMC Manual 
*
* 14  Adjusting for Measurement Errors in Predictor Variables . . . . . .199
*
*     Browne, W. J. (2009). MCMC Estimation in MLwiN, v2.26. Centre for
*     Multilevel Modelling, University of Bristol.
****************************************************************************
*     Stata do-file to replicate all analyses using runmlwin
*
*     George Leckie and Chris Charlton, 
*     Centre for Multilevel Modelling, 2012
*     https://www.bristol.ac.uk/cmm/software/runmlwin/
****************************************************************************

* 14.1 Effects of measurement error on predictors . . . . . . . . . . . .200

use "https://www.bristol.ac.uk/cmm/media/runmlwin/tutorial.dta", clear

runmlwin normexam cons standlrt, ///
	level1(student: cons) ///
	nopause

sum standlrt

set seed 12345

generate errors = sqrt(.2)*invnormal(uniform())

generate obslrt = standlrt + errors

runmlwin normexam cons errors, ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons obslrt, ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons obslrt, ///
	level1(student: cons) ///
	mcmc(me(obslrt, variances(.2))) initsprevious ///
	nopause
// Note: MLwiN does not calculate the DIC for measurement error models and so
// the the DIC is not displayed in the runmlwin output. This issue applies
// to all the models in this chapter.


* 14.2 Measurement error modelling in multilevel models . . . . . . . . .205


quietly runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	nopause

runmlwin normexam cons standlrt, ///
	level2(school: cons standlrt) ///
	level1(student: cons) ///
	mcmc(on) initsprevious ///
	nopause

quietly runmlwin normexam cons obslrt, ///
	level2(school: cons obslrt) ///
	level1(student: cons) ///
	nopause
	
matrix b = e(b)

matrix V = e(V)

runmlwin normexam cons obslrt, ///
	level2(school: cons obslrt) ///
	level1(student: cons) ///
	mcmc(on) initsb(b) initsv(V) ///
	nopause

runmlwin normexam cons obslrt, ///
	level2(school: cons obslrt) ///
	level1(student: cons) ///
	mcmc(me(obslrt, variances(.2))) initsb(b) initsv(V) ///
	nopause



* 14.3 Measurement errors in binomial models . . . . . . . . . . . . . . 208

use "https://www.bristol.ac.uk/cmm/media/runmlwin/bang1.dta", clear

runmlwin use cons age, ///
	level2(district:) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) ///
	nopause

sum age

set seed 12345

generate obsage = age + sqrt(25)*invnormal(uniform())

runmlwin use cons obsage, ///
	level2(district:) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) ///
	nopause

runmlwin use cons obsage, ///
	level2(district:) ///
	level1(woman:) ///
	discrete(distribution(binomial) link(logit) denominator(cons)) ///
	mcmc(me(obsage, variances(25))) initsprevious ///
	nopause



* 14.4 Measurement errors in more than one variable and 
*      misclassifications . . . . . . . . . . . . . . . . . . . . . . . .211

* Chapter learning outcomes . . . . . . . . . . . . . . . . . . . . . . .212





****************************************************************************
exit