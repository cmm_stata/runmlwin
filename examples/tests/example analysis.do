clear all
cd "Q:\C-modelling\runmlwin\chris"
******************************************************************************
* CONTINUOUS RESPONSES
******************************************************************************

* Single level model
use "data\tutorial.dta", clear
runmlwin normexam cons standlrt, level1(student: cons)
xtmixed normexam cons standlrt, nocons mle var


* Two-level random intercepts model
use "data\tutorial.dta", clear
runmlwin normexam cons standlrt, level2(school: cons) level1(student: cons)
xtmixed normexam cons standlrt, nocons || school: cons, nocons mle var


* Two-level random intercepts and slopes model
use "data\tutorial.dta", clear
runmlwin normexam cons standlrt, level2(school: cons standlrt) level1(student: cons)
xtmixed normexam cons standlrt, nocons || school: cons standlrt, cov(uns) nocons mle var


* Two-level random intercepts and slopes model with complex level 1
use "data\tutorial.dta", clear
gen boy = 1 - girl
runmlwin normexam cons standlrt, level2(school: cons standlrt) level1(student: boy girl)
xtmixed normexam cons standlrt, nocons || school: cons standlrt, cov(uns) nocons residuals(independent, by(girl)) mle var


* Three-level random intercepts
use "data\EG.dta", clear
runmlwin math cons year, level3(schoolid: cons) level2(childid: cons) level1(id: cons)
xtmixed math cons year, nocons || schoolid: cons, nocons || childid: cons, nocons mle var


* Three-level random intercepts and slopes and levels 3 and 2
use "data\EG.dta", clear
runmlwin math cons year, level3(schoolid: cons year) level2(childid: cons year) level1(id: cons)
mat list b
mat list V
// All goes wrong with the row and column names for the parameters!!

xtmixed math cons year, nocons || schoolid: cons year, nocons cov(uns) || childid: cons year, nocons cov(uns) mle var





******************************************************************************
* BINARY RESPONSES
******************************************************************************


* Single-level logit model (MQL1)
use "data\bang.dta", clear
runmlwin use cons, level1(woman) dist(binomial) denom(cons)

* Single-level probit model (MQL1)
use "data\bang.dta", clear
runmlwin use cons, level1(woman) dist(binomial) link(probit) denom(cons)

* Single-level cloglog model (MQL1)
use "data\bang.dta", clear
runmlwin use cons, level1(woman) dist(binomial) link(cloglog) denom(cons)

* Two-level random intercepts logit model (MQL1)
use "data\bang.dta", clear
runmlwin use cons, level2(district: cons) level1(woman) dist(binomial) denom(cons)

* Two-level random intercepts probit model (MQL1)
use "data\bang.dta", clear
runmlwin use cons, level2(district: cons) level1(woman) dist(binomial) link(probit) denom(cons)

* Two-level random intercepts cloglog model (MQL1)
use "data\bang.dta", clear
runmlwin use cons, level2(district: cons) level1(woman) dist(binomial) link(cloglog) denom(cons)





******************************************************************************
* IF STATEMENTS
******************************************************************************
use "data\tutorial.dta", clear

* Single level model (Girls only)
runmlwin normexam cons standlrt if girl==1, level1(student: cons)
xtmixed normexam cons standlrt if girl==1, nocons mle var





******************************************************************************
* IN STATEMENTS
******************************************************************************
use "data\tutorial.dta", clear

* Single level model (first 1000 observations only)
runmlwin normexam cons standlrt in 1/1000, level1(student: cons)
xtmixed normexam cons standlrt if 1/1000, nocons mle var
// COMMANDS GIVE DIFFEREMT RESULTS: THE OBS GET SORTED AT SOME POINT





******************************************************************************
* DO WALD TESTS
******************************************************************************
use "data\tutorial.dta", clear
gen boy = 1 - girl
gen boysch = (schgend==2)
gen girlssch = (schgend==3)



runmlwin normexam cons standlrt girl, level2(school: cons standlrt) level1(student: cons)
test girl=0.2
xtmixed normexam cons standlrt girl, nocons || school: cons standlrt, cov(uns) nocons mle var
test girl=0.2



runmlwin normexam cons standlrt girl boysch girlssch, level2(school: cons standlrt) level1(student: cons)
test boysch==girlssch
xtmixed normexam cons standlrt girl boysch girlssch, nocons || school: cons standlrt, cov(uns) nocons mle var
test boysch==girlssch




runmlwin normexam cons standlrt girl, level2(school: cons) level1(student: cons)
test [RP2]v(cons) = 0
test [RP1]v(cons) = 0

xtmixed normexam cons standlrt girl, nocons || school: cons, cov(uns) nocons mle var
estimates store temp
nlcom exp([lns1_1_1]_cons)^2, post
di (_b[_nl_1]/_se[_nl_1])^2
estimates restore temp
nlcom exp([lnsig_e]_cons)^2, post
di (_b[_nl_1]/_se[_nl_1])^2




runmlwin normexam cons standlrt girl, level2(school: cons) level1(student: boy girl)
test [RP1]v(boy) = 0
test [RP1]v(girl) = 0
test [RP1]v(boy) = [RP1]v(girl)

xtmixed normexam cons standlrt girl, nocons || school: cons, cov(uns) residuals(independent, by(girl)) nocons mle var
estimates store temp
nlcom exp([lnsig_e]_cons)^2, post
di (_b[_nl_1]/_se[_nl_1])^2
estimates restore temp
nlcom exp([lnsig_e]_cons + [r_lns2ose]_cons)^2, post
di (_b[_nl_1]/_se[_nl_1])^2
estimates restore temp
nlcom exp([lnsig_e]_cons )^2 - exp([lnsig_e]_cons + [r_lns2ose]_cons)^2, post
di (_b[_nl_1]/_se[_nl_1])^2






******************************************************************************
* FIXED PART MODEL PREDICTIONS (based on first equation (FP) only)
******************************************************************************
use "data\tutorial.dta", clear
gen boy = 1 - girl

runmlwin normexam cons standlrt girl, level1(student: cons)
predict y_mlwin
xtmixed normexam cons standlrt girl, nocons mle var
predict y_stata

tabstat y_mlwin y_mlwin, by(girl)
drop y_mlwin y_stata


* Two-level random intercepts and slopes model with complex level 1
runmlwin normexam cons standlrt, level2(school: cons standlrt) level1(student: boy girl)
predict y_mlwin
xtmixed normexam cons standlrt, nocons || school: cons standlrt, cov(uns) nocons residuals(independent, by(girl)) mle var
predict y_stata

tabstat y_mlwin y_mlwin, by(girl)
drop y_mlwin y_mlwin






******************************************************************************
* STORE MODELS
******************************************************************************
use "data\tutorial.dta", clear

runmlwin normexam cons standlrt, level1(student: cons)
estimates store Model1
// THIS COMMAND SHOULD WORK



******************************************************************************
* DO LR TESTS
******************************************************************************
// WHERE IS LOG-LIKELEHOOD STORED IN MLWIN. IT IS NOT IN THE WORKSHEET. IS IT IN A BOX?
// IT NEEDS TO BE SAVED AS DATA





******************************************************************************
* CALL MLWIN
******************************************************************************
* Open MLwiN and display the saved model
//shell "C:\Program Files (x86)\MLwiN v2.20\mlwin.exe" "`c(pwd)'\\_model.wsz" /run "`c(pwd)'/_wset.txt"

******************************************************************************
exit
