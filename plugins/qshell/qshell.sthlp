{smcl}
{* *! version 0.0.1  17feb2011}{...}
{cmd:help qshell}
{hline}

{title:Title}

{p2colset 5 18 20 2}{...}
{p2col :qshell {hline 2}}Temporarily invoke operating system without taking focus{p_end}
{p2colreset}{...}


{title:Syntax}

{p 8 12 2}{c -(}{opt qsh:ell}{c )-} [{it:operating_system_command}]

{title:Description}

{pstd}
{opt qshell} allows you to send commands to your operating system.
Stata will wait for the shell to close or the {it:operating_system_command}
to complete before continuing.

{title:Examples}

{p2colset 5 39 41 2}{...}

{p2col :{cmd:. qshell erase *.log}}(Windows){p_end}

{p2col :{cmd:. qshell edit myfile.ado}}(assuming editor called {opt edit}){p_end}

{title:Also see}

{psee}
Help:  {manhelp shell D}
