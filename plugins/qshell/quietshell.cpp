#include "windows.h"
#include "stplugin.h"

        STDLL stata_call(int argc, char *argv[])
        {
			//SF_display("Attempting to run ");
			//SF_display(argv[0]);
			//SF_display("\n");

        	//for(int i=0;i < argc; i++) {
        	//	SF_display(argv[i]) ;
        	//	SF_display("\n") ;
        	//}


			wchar_t cmdline[32768];
			cmdline[0] = '\0';

			SF_display("Attempting to run ");
			for (int i = 0; i < argc; i++){
				SF_display(argv[i]);
				//SF_display(" ");
				size_t strlen = mbstowcs(NULL, argv[i], 0) + 1;
				wchar_t *lpfile = new wchar_t[strlen];
				size_t size = mbstowcs(lpfile,  argv[i], strlen);
				if (size == (size_t) (-1)) {
					SF_error("Error in plugin argument");
				}else{
					wcscat(cmdline, lpfile);
					//wcscat(cmdline, L" ");
				}
				delete [] lpfile;
			}
			SF_display("\n");
			

			//HINSTANCE nResult = ShellExecute(NULL, L"open", lpfile, NULL, NULL, SW_SHOWNOACTIVATE);
			//if (int(nResult) <= 32) {
			//	SF_display("Command failed to run");
			//	return 0
			//}

			// Alternative using ShellExecuteEx
			//SHELLEXECUTEINFO sei = { sizeof(sei) };
			//sei.fMask = SEE_MASK_NOCLOSEPROCESS;
			//sei.nShow = SW_SHOWNA;
			//sei.lpVerb = L"Open";
			//sei.lpFile = cmdline;
			//ShellExecuteEx(&sei);

			//if (int(sei.hInstApp) <= 32) {
			//	SF_display("Command failed to run");
			//	return 0;
			//}

			//while (TRUE) {
			//	DWORD id = MsgWaitForMultipleObjects(1, &sei.hProcess, FALSE, INFINITE, QS_ALLINPUT);
			//	if (id == WAIT_OBJECT_0 + 1) { // Message passed, still running
			//		MSG msg;
			//		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			//			TranslateMessage(&msg);
			//			DispatchMessage(&msg);
			//		}
			//	} else {
			//		break; // correct object or failure
			//		//if (id == WAIT_OBJECT_0) {
			//		//	break;
			//		//} else {
			//		//	break;
			//		//}
			//	}
			//}

			//DWORD ExitCode = 0;
			//GetExitCodeProcess(sei.hProcess, &ExitCode);
			//CloseHandle( sei.hProcess );



			STARTUPINFO startupInfo = {0};
			startupInfo.cb = sizeof(startupInfo);
			startupInfo.dwFlags = STARTF_USESHOWWINDOW | STARTF_FORCEOFFFEEDBACK;
			startupInfo.wShowWindow = SW_SHOWNOACTIVATE;

			PROCESS_INFORMATION processInformation;

			if (!CreateProcess(NULL, cmdline, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &startupInfo, &processInformation)){
				SF_error("Command failed to run");
				return 0;
			}
			
			while (TRUE) {
				DWORD id = MsgWaitForMultipleObjects(1, &processInformation.hProcess, FALSE, INFINITE, QS_ALLINPUT);
				if (id == WAIT_OBJECT_0 + 1) { // Message passed, still running
					MSG msg;
					while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
				} else {
					break; // correct object or failure
					//if (id == WAIT_OBJECT_0) {
					//	break;
					//} else {
					//	break;
					//}
				}
				SF_poll();
				if(SW_stopflag) {
					CloseHandle( processInformation.hProcess );
					CloseHandle( processInformation.hThread );
					return(SW_stopflag);
				}
			}

			//WaitForSingleObject( processInformation.hProcess, INFINITE );

			//while (WaitForSingleObject( processInformation.hProcess, 500 /*INFINITE*/ ) != 0){
			//	SF_display(".");
			//}
			DWORD ExitCode = 0;
			GetExitCodeProcess(processInformation.hProcess, &ExitCode);
			CloseHandle( processInformation.hProcess );
			CloseHandle( processInformation.hThread );

            return(ExitCode) ;
        }
