#include <vector>
#include <windows.h>
#include <delayimp.h>
#include <stdio.h>
#include "stplugin.h"


#pragma pack(1)
struct Message {
	int code;
	int arg1;
	int arg2;
	int arg3;
};
#pragma pack()

extern "C" __declspec(dllimport) long int __stdcall  MLN_InitEx2(int sheetsize);
extern "C" __declspec(dllimport) long int __stdcall  MLN_UnInit();
extern "C" __declspec(dllimport) long int __stdcall  MLN_ExecuteCommand(const char *command);
extern "C" __declspec(dllimport) long int __stdcall  MLN_RespondToRequest(Message *M, void *Response);
extern "C" __declspec(dllimport) long int __stdcall  MLN_ResetAffected();
extern "C" __declspec(dllimport) void __stdcall MLN_set_displaymsgEx(void* function);
extern "C" __declspec(dllimport) void __stdcall MLN_set_displayoutput(void* function);
extern "C" __declspec(dllimport) void __stdcall MLN_set_promptuserEx(void* function);
extern "C" __declspec(dllimport) void __stdcall MLN_set_burnin_percent(void* function);
extern "C" __declspec(dllimport) void __stdcall MLN_set_writeoutputRTF(void* function);
extern "C" __declspec(dllimport) void __stdcall MLN_set_displayoutputRTF(void* function);
extern "C" __declspec(dllimport) void __stdcall MLN_set_displayoutputTable(void* function);
extern "C" __declspec(dllimport) void __stdcall MLN_set_displayhelp(void* function);
extern "C" __declspec(dllimport) void __stdcall MLN_set_exitapp(void* function);
extern "C" __declspec(dllimport) void __stdcall MLN_set_updateeqn(void* function);
extern "C" __declspec(dllimport) void __stdcall MLN_set_updatewindows(void* function);
extern "C" __declspec(dllimport) void __stdcall MLN_set_yesno(void *vbfunction);
extern "C" __declspec(dllimport) void __stdcall MLN_set_warnmsg(void *vbfunction);

FARPROC WINAPI delayHook(unsigned dliNotify, PDelayLoadInfo pdli)
{
    switch (dliNotify) {
        case dliStartProcessing :

            // If you want to return control to the helper, return 0.
            // Otherwise, return a pointer to a FARPROC helper function
            // that will be used instead, thereby bypassing the rest 
            // of the helper.

            break;

        case dliNotePreLoadLibrary :

            // If you want to return control to the helper, return 0.
            // Otherwise, return your own HMODULE to be used by the 
            // helper instead of having it call LoadLibrary itself.

            break;

        case dliNotePreGetProcAddress :

            // If you want to return control to the helper, return 0.
            // If you choose you may supply your own FARPROC function 
            // address and bypass the helper's call to GetProcAddress.

            break;

        case dliFailLoadLib : 

            // LoadLibrary failed.
            // If you don't want to handle this failure yourself, return 0.
            // In this case the helper will raise an exception 
            // (ERROR_MOD_NOT_FOUND) and exit.
            // If you want to handle the failure by loading an alternate 
            // DLL (for example), then return the HMODULE for 
            // the alternate DLL. The helper will continue execution with 
            // this alternate DLL and attempt to find the
            // requested entrypoint via GetProcAddress.

            break;

        case dliFailGetProc :

            // GetProcAddress failed.
            // If you don't want to handle this failure yourself, return 0.
            // In this case the helper will raise an exception 
            // (ERROR_PROC_NOT_FOUND) and exit.
            // If you choose you may handle the failure by returning 
            // an alternate FARPROC function address.


            break;

        case dliNoteEndProcessing : 

            // This notification is called after all processing is done. 
            // There is no opportunity for modifying the helper's behavior
            // at this point except by longjmp()/throw()/RaiseException. 
            // No return value is processed.

            break;

        default :

            return NULL;
    }

    return NULL;
}

PfnDliHook __pfnDliNotifyHook2 = delayHook;
static int MLN_loaded = 0;
static bool unload = false;
static char out[1000000] = "";
static wchar_t libpath[MAX_PATH];

void GUI_Refresh(){
	MSG msg;
	while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

int __stdcall display_message(BSTR str){
	char buff[10000];
	WideCharToMultiByte(CP_ACP, 0, str, -1, buff, 10000, NULL, NULL);
	SF_display(buff);
	GUI_Refresh();
	return 0;
}

void __stdcall display_output(BSTR str){
	char buff[10000];
	WideCharToMultiByte(CP_ACP, 0, str, -1, buff, 10000, NULL, NULL);
	SF_display(buff);
	GUI_Refresh();
}

void __stdcall prompt_user(BSTR str){
	char buff[10000];
	WideCharToMultiByte(CP_ACP, 0, str, -1, buff, 10000, NULL, NULL);
	SF_display(buff);
	GUI_Refresh();
}

void __stdcall burnin_percent(int p){
	char buff[1000];
	sprintf(buff, "BURNING IN %d%% done\n", p);
	SF_display(buff);
	GUI_Refresh();
}

void __stdcall write_output_rtf(BSTR str){
	char buff[10000];
	WideCharToMultiByte(CP_ACP, 0, str, -1, buff, 10000, NULL, NULL);
	strcat(out, buff);
}

void __stdcall display_output_rtf(){
	SF_display(out);
	out[0] = '\0';
	GUI_Refresh();
}

void __stdcall write_output_table(int nrows, int ncols){
	SF_display(out);
	out[0] = '\0';
	GUI_Refresh();
}

void __stdcall display_help(BSTR str){
	char buff[10000];
	WideCharToMultiByte(CP_ACP, 0, str, -1, buff, 10000, NULL, NULL);
	SF_display(buff);
	GUI_Refresh();
}

void __stdcall exitApp(){
	unload = true;
	SF_display("Bye");
}

void __stdcall update_equation(){
	SF_display("");
	GUI_Refresh();
}

void __stdcall update_windows(){
	SF_display("");
	GUI_Refresh();
}

int __stdcall yesno(BSTR str){
	char buff[10000];
	WideCharToMultiByte(CP_ACP, 0, str, -1, buff, 10000, NULL, NULL);
	SF_display(buff);
	SF_display("yes");
	GUI_Refresh();
	return 1;
}

void __stdcall warnmsg(BSTR str){
	char buff[10000];
	WideCharToMultiByte(CP_ACP, 0, str, -1, buff, 10000, NULL, NULL);
	SF_display(buff);
	GUI_Refresh();
}

int pauseState() {
	const short int VID_PAUSESTATE = 127;
	Message m;
	m.code = VID_PAUSESTATE;
	int pausestate = 0;
	int temp = MLN_RespondToRequest(&m, &pausestate);
	return pausestate;
}

void command(const char* cmd){
	const short int REQ_ERRMSG = 20;
	int ret = MLN_ExecuteCommand(cmd);
	if (ret != 10) { //STA_READY
		char errmsg[20000];
		Message m;
		m.code = REQ_ERRMSG;
		int temp = MLN_RespondToRequest(&m, &errmsg);
		SF_error(errmsg);
	}
	GUI_Refresh();
}

STDLL stata_call(int argc, char *argv[])
{
	int cmdlen = strlen(argv[0]) + 1;
	std::vector<char> buff(cmdlen);
	//char buff[512];
	//SF_display("MLN Plugin Version 1.00\n");
	if (MLN_loaded == 0){
		wchar_t strExePath[MAX_PATH];
		GetModuleFileName(GetModuleHandle(L"MLN_Command.plugin"), strExePath, MAX_PATH);
		wchar_t drive[_MAX_DRIVE];
		wchar_t dir[_MAX_DIR];
		wchar_t fname[_MAX_FNAME];
		wchar_t ext[_MAX_EXT];
		
		_wsplitpath_s(strExePath, drive, dir, fname, ext);
		swprintf(libpath, L"%s%s", drive, dir);
		//SF_display("Attempting to load MLN plugin from: ");
		//wcstombs(buff, libpath, sizeof(buff));
		//SF_display(buff);
		//SF_display("\n");
		SetDllDirectory(libpath);

		SF_display("Initialising MLN plugin\n");
		MLN_set_displaymsgEx(*display_message);
		MLN_set_displayoutput(*display_output);
		MLN_set_promptuserEx(*prompt_user);
		MLN_set_burnin_percent(*burnin_percent);
		MLN_set_writeoutputRTF(*write_output_rtf);
		MLN_set_displayoutputRTF(*display_output_rtf);
		MLN_set_displayoutputTable(*write_output_table);
		MLN_set_displayhelp(*display_help);
		MLN_set_exitapp(*exitApp);
		MLN_set_updateeqn(*update_equation);
		MLN_set_updatewindows(*update_windows);
		MLN_set_yesno(*yesno);
		MLN_set_warnmsg(*warnmsg);

		MLN_InitEx2(50000);
		SetDllDirectory(NULL);
		MLN_loaded = 1;
	}
	if (argc != 1){
		SF_display("Parameters: MLN_Command <commandline>\n");
		return(198);
	}
	sprintf_s(&buff[0], buff.size(), "%s", argv[0]);
	SetDllDirectory(libpath);
	command(&buff[0]);
	MLN_ResetAffected();
	while (pauseState() > 1) {
		SF_display(".");
		SF_poll();
		if(SW_stopflag) {
			command("ABORT");
			SetDllDirectory(NULL);
			return(SW_stopflag);
		}
		GUI_Refresh();
		command("RESUME");
		MLN_ResetAffected();
	}

	if (pauseState() == 0) {
		SF_display("Execution completed");
	}

	if (pauseState() == 1) {
		SF_display("Execution paused");
	}
	
	SetDllDirectory(NULL);
	if (unload == true){
		SetDllDirectory(libpath);
		BOOL res = __FUnloadDelayLoadedDLL2("winmln6.dll");
		SetDllDirectory(NULL);
		unload = false;
		if (res) {
			MLN_loaded = 0;
		}
	}
	return(0) ;
}

