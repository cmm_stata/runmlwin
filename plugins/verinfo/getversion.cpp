#include "windows.h"
#include "winver.h"
#include "stplugin.h"

        STDLL stata_call(int argc, char *argv[])
        {
			if (argc != 5) {
				SF_error("Invalid Arguments");
			}

			size_t strlen = mbstowcs(NULL, argv[0], 0) + 1;
			wchar_t *lpfile = new wchar_t[strlen];
			size_t newsize = mbstowcs(lpfile,  argv[0], strlen);

			DWORD size = GetFileVersionInfoSize(lpfile, NULL);
			BYTE* versionInfo = new BYTE[size];
			if (!GetFileVersionInfo(lpfile, NULL, size, versionInfo))
			{
				delete[] versionInfo;
				SF_error("No version information found");
				return 198;
			}

			UINT len = 0;
			VS_FIXEDFILEINFO*   vsfi = NULL;
			VerQueryValue(versionInfo, L"\\", (void**)&vsfi, &len);

			ST_retcode rc;
			if(rc = SF_scal_save(argv[1], HIWORD(vsfi->dwFileVersionMS))) return(rc) ; 
			if(rc = SF_scal_save(argv[2], LOWORD(vsfi->dwFileVersionMS))) return(rc) ; 
			if(rc = SF_scal_save(argv[3], HIWORD(vsfi->dwFileVersionLS))) return(rc) ; 
			if(rc = SF_scal_save(argv[4], LOWORD(vsfi->dwFileVersionLS))) return(rc) ; 

			delete[] versionInfo;

            return 0;
        }
